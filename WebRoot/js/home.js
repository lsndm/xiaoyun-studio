var force = d3.forceSimulation();
/**
 * 网页加载完成时调用
 */
function window_load(){
	var display = d3.select("#navbar-button").style("display");
	if(display!="none"){
		d3.select("#map>svg").on("click",svg_click);
	}
	if(window.sessionStorage){
		var mapData = window.sessionStorage.mapData;
		if(mapData!=null && mapData!='null'){
			var data = JSON.parse(mapData);
			initMap(data);
		}else{
			d3.json("v1/map", function(data){
				window.sessionStorage.mapData = JSON.stringify(data);
				initMap(data);
			});
		}
	}else{
		d3.json("v1/map", initMap);
	}
}

/**
 * 初始化地图
 */
function initMap(data) {
	var nodes = data.rows[0].nodes;
	var links = data.rows[0].links;
	var width = document.body.clientWidth;
	var height = document.body.clientHeight;
	var color = d3.scaleOrdinal(d3.schemeCategory20);
	//初始化地图
	force.force("link",d3.forceLink().strength(0.3).distance(120).iterations(10).id(function(d) {return d.id;}));
	force.force("charge", d3.forceManyBody().strength(-500));
	force.force("center", d3.forceCenter(width / 2, height / 2));
	var svg = d3.select("#map>svg").attr("width", width).attr("height", height);
	var svg_rect = svg.append("rect");
	var svg_g = svg.append("g");
	svg_rect.attr("width", width).attr("height", height).style("fill", "none").style("pointer-events", "all");
	//使地图可以拖动和缩放
    svg_rect.call(d3.zoom().scaleExtent([1 / 2, 4]).on("zoom", function(){
    	svg_g.attr("transform", d3.event.transform);
    }));
	//添加连线
	var svg_link = svg_g.selectAll("line").data(links).enter().append("line").style(
			"stroke", "#ccc").style("stroke-width", 1);
	//添加超链接
	var svg_a = svg_g.selectAll("a").data(nodes).enter().append("a").attr(
			"xlink:href", function(d) {
				return d.url;
			});
	//添加节点
	var svg_circle = svg_a.append("circle").attr("r", 20).attr("fill",
			function(d, i) {
				return color(i);
			});
	//使节点可拖动
	svg_circle.call(d3.drag().on("start", dragStart).on("drag", dragged).on("end",dragEnd));
	// 添加节点的文字
	var svg_text = svg_g.selectAll("text").data(nodes).enter().append("text")
			.style("fill", "black").attr("dx", 21).attr("dy", 8).text(
					function(d) {
						return d.title;
					});
	force.nodes(nodes).on("tick", function(){
		tick(svg_link,svg_circle,svg_text);
	});
	force.force("link").links(links);
}

//每隔一段时间都会触发一次
function tick(svg_link,svg_circle,svg_text) {
	//更新连线的坐标
	svg_link.attr("x1", function(d) {
		return d.source.x;
	}).attr("y1", function(d) {
		return d.source.y;
	}).attr("x2", function(d) {
		return d.target.x;
	}).attr("y2", function(d) {
		return d.target.y;
	});
    //更新节点的坐标
	svg_circle.attr("cx", function(d) {
		return d.x;
	}).attr("cy", function(d) {
		return d.y;
	});
	// 更新文字的坐标
	svg_text.attr("x", function(d) {
		return d.x;
	}).attr("y", function(d) {
		return d.y;
	});
}
/**
 * 节点拖动开始
 */
function dragStart(d) {
	if (!d3.event.active)
		force.alphaTarget(0.3).restart();
	d.fx = d.x;
	d.fy = d.y;
}
/**
 * 节点拖动中
 */
function dragged(d) {
	d.fx = d3.event.x;
	d.fy = d3.event.y;
}
/**
 * 节点拖动结束
 */
function dragEnd(d) {
	if (!d3.event.active)
		force.alphaTarget(0);
	d.fx = null;
	d.fy = null;
}
/**
 * svg标签的点击事件
 */
function svg_click() {
	var navbar = d3.select("#navbar");
	if (navbar.style("display") != "none") {
		navbar.style("display", "none");
	}
}
/**
 * 菜单条上汉堡菜单的点击事件
 */
function navbar_button_click() {
	var navbar = d3.select("#navbar");
	if (navbar.style("display") == "none") {
		navbar.style("display", "block");
	} else {
		navbar.style("display", "none");
	}
}
