/**
 * 导航菜单的点击事件
 * @param id 菜单的ID
 */
function navClick(id) {
	var display = $("#" + id).next().children().css("display");
	if (display == 'none') {
		$("#navbar .glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-right");
		$("#" + id + " .glyphicon").addClass("glyphicon-menu-down");
		$("#navbar .nav-twoUl").hide();
		$("#" + id).next().children().show("fast");
	} else {
		$("#" + id + " .glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-right");
		$("#" + id).next().children().hide("fast");
	}
}