/**
 * 添加数据
 */
function addData() {
    
}
/**
 * 编辑数据
 */
function editData() {
    
}
/**
 * 删除数据
 */
function deleteData() {
    toastr.options = {"positionClass": "toast-top-center"};
    var rows = $('#table').bootstrapTable('getSelections');
    if (rows.length == 0) {
        toastr.info("请至少选择一行！");
        return;
    }
    var roleId = $.map(rows, function (item) {
        return item.roleId;
    });
    bootbox.confirm({
        title: "系统提示",
        message: "确定要删除吗？",
        buttons: {
            confirm: {
                label: '<i class="fa fa-check"></i> 确定'
            },
            cancel: {
                label: '<i class="fa fa-times"></i> 取消'
            }
        },
        callback: function (result) {
            if(result){
                deleteData_ajax(roleId);
            }
        }
    });
}

function deleteData_ajax(roleId) {
    $.ajax({
        type:'delete',
        url:'../../v1/role',
        data:{roleId:JSON.stringify(roleId),format:'json'},
        success:function (data) {
            toastr.success("保存成功");
            $('#table').bootstrapTable('refresh');
        },error:function (data) {
            if (data.responseJSON && data.responseJSON.error) {
                toastr.error(data.responseJSON.error.message);
            }else{
                toastr.error("系统错误，请稍后重试");
            }
        }
    });
}

