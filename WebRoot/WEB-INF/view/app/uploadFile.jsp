<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <%@ include file="../header.jsp" %>
  <script src="../plugins/bootstrap/js/bootstrap.min.js"></script>
  <link href="../plugins/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet">
  <script src="../plugins/bootstrap-fileinput/js/fileinput.min.js"></script>
  <script src="../plugins/bootstrap-fileinput/js/locales/zh.js"></script>
  <title>上传文件-小云工作室</title>
</head>
<body>
<div class="container">
  <div class="row">
    <div class="col-sm-6">
      <h1 class="text-center">上传文件</h1>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-9">
      <form role="form" id="form">
        <div class="form-group">
          <label for="uploadFile">上传文件(支持jpg,gif,png):</label>
          <input id="uploadFile" type="file" name="file" class="file">
        </div>
      </form>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-9" id="preview">

    </div>
  </div>
</div>
<script type="text/javascript">
  var uploadFile = $("#uploadFile");
  //初始化参数
  uploadFile.fileinput({
    uploadUrl: '../v1/file/upload',  //上传url
    language: 'zh',    //语言
    dataType: 'json',   //数据类型
    showPreview: false,  //是否预览文件，以及显示预览框
    showRemove: false,   //是否显示移除按钮
    maxFileSize:1024, //最大文件大小，单位是KB
    showCaption: true,  //是否显示一个文本框，文本框中会显示文件名以及错误提示
    uploadExtraData: {format: 'json'},  //上传时附加的请求参数
    allowedFileExtensions: ['jpg', 'gif', 'png']  //允许的文件扩展名
  });
  //文件上传之前调用
  uploadFile.on('filebatchpreupload', function(event, data, previewId, index) {
     $('#form>.message').remove();
  });
  //上传成功事件，如果上传多个文件，每个文件上传成功后都会调用一次
  uploadFile.on('fileuploaded', function (event, data, previewId, index) {
    var html = '<p>上传成功!</P><img src="..'+data.response.rows[0].url+'" class="img-responsive">';
    $('#preview').empty().append(html);
  });
  //文件上传失败后触发
  uploadFile.on('fileuploaderror', function(event, data, msg) {
    var html = '<div class="form-group message"><p class="alert alert-danger">文件上传失败:'+data.filenames[data.index]+'</p></div>';
    $('#form').append(html);
  });
  //所有文件都上传完成后触发
  uploadFile.on('filebatchuploadcomplete', function(event, files, extra) {
    uploadFile.fileinput('clear'); //清除选择的文件
  });
</script>
</body>
</html>
