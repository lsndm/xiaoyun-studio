<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <%@ include file="../header.jsp" %>
  <title>应用-小云工作室</title>
</head>
<body>
<div class="container">
  <div class="row">
    <div class="col-sm-6">
      <h1 class="text-center">应用</h1>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-9">
      <p><a href="uploadFile.html">上传文件</a></p>
      <p><a href="preview.html">文件预览</a></p>
    </div>
  </div>
</div>
</body>
</html>
