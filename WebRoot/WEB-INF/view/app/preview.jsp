<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <%@ include file="../header.jsp" %>
    <title>文件预览-小云工作室</title>
    <script src="../plugins/bootstrap/js/bootstrap.min.js"></script>
    <link href="../plugins/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet">
    <script src="../plugins/bootstrap-fileinput/js/fileinput.min.js"></script>
    <script src="../plugins/bootstrap-fileinput/js/locales/zh.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <h1 class="text-center">文件预览</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-9">
            <form role="form" id="form"  method="post" action="../v1/file/preview" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="uploadFile">选择文件(支持doc,docx,xls):</label>
                    <input id="uploadFile" type="file" name="file" class="file">
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    var uploadFile = $("#uploadFile");
    //初始化参数
    uploadFile.fileinput({
        language: 'zh',    //语言
        showPreview: false,  //是否预览文件，以及显示预览框
        showRemove: false,   //是否显示移除按钮
        maxFileSize:2048, //最大文件大小，单位是KB
        showCaption: true,  //是否显示一个文本框，文本框中会显示文件名以及错误提示
        allowedFileExtensions: ['doc', 'docx', 'xls']  //允许的文件扩展名
    });
</script>
</body>
</html>
