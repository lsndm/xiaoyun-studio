<%@ page pageEncoding="utf-8" session="false"%>
<%
  String contextPath = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
%>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="<%=basePath%>favicon.ico" type="image/x-icon" />
<link rel="icon" sizes="any" href="<%=basePath%>img/logo/logo.svg" />
<link href="<%=basePath%>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="<%=basePath%>plugins/jquery/jquery.min.js"></script>
