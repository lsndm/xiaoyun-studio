<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false" %>
<%--本页是导航菜单，在其它页面通过<%include%>引用 注意每个菜单的ID，“_”前边应该是父菜单的ID--%>
<div class="col-lg-3">
  <nav id="navbar">
    <ul class="nav nav-pills nav-stacked nav-oneUl">
      <li class="subNav" id="abstract">
        <a href="javascript:navClick('abstract')">简介<span class="glyphicon glyphicon-menu-right"></span></a></li>
      <li>
        <ul class="nav nav-twoUl">
          <li id="abstract_a"><a href="#">二级</a></li>
          <li id="abstract_b"><a href="#">二级</a></li>
          <li id="abstract_c"><a href="#">二级</a></li>
          <li id="abstract_d"><a href="#">二级</a></li>
        </ul>
      </li>
      <li class="subNav" id="api">
        <a href="javascript:navClick('api')">API <span class="glyphicon glyphicon-menu-right"></span></a></li>
      <li>
        <ul class="nav nav-twoUl">
          <li id="api_a"><a href="#">二级</a></li>
          <li id="api_b"><a href="#">二级</a></li>
          <li id="api_c"><a href="#">二级</a></li>
          <li id="api_d"><a href="#">二级</a></li>
        </ul>
      </li>
    </ul>
  </nav>
</div>
<%--
<script type="text/javascript">
  var navID = '<%=request.getParameter("navID")%>';
  var parentID = navID.substring(0, navID.indexOf("_"));
  $("#" + parentID).css("background-color", "#eee");
  $("#" + parentID + " .glyphicon").addClass("glyphicon-menu-down");
  $("#" + parentID).next().children().show();
  $("#" + navID).css("background-color", "#eee");
</script>--%>
