<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <%@ include file="../head.jsp" %>
  <title>权限管理-小云工作室</title>
  <link rel="stylesheet" href="../../plugins/bootstrap-table/bootstrap-table.min.css">
</head>
<body class="hold-transition skin-blue-light sidebar-mini">
<div class="wrapper">
  <jsp:include page="../header.jsp">
    <jsp:param name="menuId" value="authManage"/>
  </jsp:include>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>权限管理</h1>
      <ol class="breadcrumb">
        <li><a href="../index"><i class="fa fa-home"></i></a></li>
        <li><a href="#">安全管理</a></li>
        <li class="active">权限管理</li>
      </ol>
    </section>
    <section class="content">
      <div id="toolbar"></div>
      <table id="table" data-toggle="table" data-url="../../v1/permission" data-toolbar="#toolbar">
        <thead>
        <tr>
          <th data-field="permissionId" data-visible="false">主键</th>
          <th data-field="name" data-sortable="true" data-searchable="true">名称</th>
          <th data-field="type" data-sortable="true">类型</th>
          <th data-field="description" data-searchable="true">描述</th>
        </tr>
        </thead>
      </table>
    </section>
  </div>
</div>
<script src="../../plugins/bootstrap-table/bootstrap-table.min.js"></script>
<script src="../../plugins/bootstrap-table/bootstrap-table-xiaoyun.js"></script>
</body>
</html>
