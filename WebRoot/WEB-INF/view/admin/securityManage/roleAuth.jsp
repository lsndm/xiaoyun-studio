<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <%@ include file="../head.jsp" %>
  <title>角色权限-小云工作室</title>
  <link rel="stylesheet" href="../../plugins/bootstrap-table/bootstrap-table.min.css">
  <link rel="stylesheet" href="../../plugins/select2/select2.min.css">
  <link rel="stylesheet" href="../../plugins/select2/select2-bootstrap.css">
  <link rel="stylesheet" href="../../css/admin/base.css">
</head>
<body class="hold-transition skin-blue-light sidebar-mini">
<div class="wrapper">
  <jsp:include page="../header.jsp">
    <jsp:param name="menuId" value="roleAuth"/>
  </jsp:include>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>角色权限
        <small>添加或者删除角色的权限</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="../index"><i class="fa fa-home"></i></a></li>
        <li><a href="#">安全管理</a></li>
        <li class="active">角色权限</li>
      </ol>
    </section>
    <section class="content">
      <div>请选择一个角色：<select id="roleSelect"></select></div>
      <div id="toolbar">
        <button class="btn btn-default" onclick="editData()"><i class="glyphicon glyphicon-floppy-disk"></i> 保存</button>
      </div>
      <table id="table" data-toggle="table" data-url="../../v1/permission" data-toolbar="#toolbar" data-page-size="300">
        <thead>
        <tr>
          <th data-checkbox="true"></th>
          <th data-field="permissionId" data-visible="false">主键</th>
          <th data-field="name" data-sortable="true" data-searchable="true">名称</th>
          <th data-field="type" data-sortable="true">类型</th>
          <th data-field="description" data-searchable="true">描述</th>
        </tr>
        </thead>
      </table>
    </section>
  </div>
</div>
<script src="../../plugins/bootstrap-table/bootstrap-table.min.js"></script>
<script src="../../plugins/bootstrap-table/bootstrap-table-xiaoyun.js"></script>
<script src="../../plugins/bootbox/bootbox.js"></script>
<script src="../../plugins/select2/select2.js"></script>
<script src="../../plugins/select2/select2-xiaoyun.js"></script>
<script src="../../js/admin/securityManage/roleAuth.js"></script>
</body>
</html>
