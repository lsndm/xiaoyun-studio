<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <%@ include file="../head.jsp" %>
  <title>角色管理-小云工作室</title>
  <link rel="stylesheet" href="../../plugins/bootstrap-table/bootstrap-table.min.css">
  <link rel="stylesheet" href="../../css/admin/base.css">
</head>
<body class="hold-transition skin-blue-light sidebar-mini">
<div class="wrapper">
  <jsp:include page="../header.jsp">
    <jsp:param name="menuId" value="roleManage"/>
  </jsp:include>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>角色管理</h1>
      <ol class="breadcrumb">
        <li><a href="../index"><i class="fa fa-home"></i></a></li>
        <li><a href="#">安全管理</a></li>
        <li class="active">角色管理</li>
      </ol>
    </section>
    <section class="content">
      <div id="toolbar">
        <button class="btn btn-default" onclick="addData()"><i class="fa fa-plus"></i> 新建</button>
        <button class="btn btn-default" onclick="editData()"><i class="fa fa-pencil"></i> 编辑</button>
        <button class="btn btn-default" onclick="deleteData()"><i class="fa fa-remove"></i> 删除</button>
      </div>
      <table id="table" data-toggle="table" data-url="../../v1/role" data-toolbar="#toolbar">
        <thead>
        <tr>
          <th data-checkbox="true"></th>
          <th data-field="roleId" data-visible="false">主键</th>
          <th data-field="name" data-sortable="true" data-searchable="true">名称</th>
          <th data-field="description" data-searchable="true">描述</th>
        </tr>
        </thead>
      </table>
    </section>
  </div>
</div>
<script src="../../plugins/bootstrap-table/bootstrap-table.min.js"></script>
<script src="../../plugins/bootstrap-table/bootstrap-table-xiaoyun.js"></script>
<script src="../../js/admin/securityManage/roleManage.js"></script>
<script src="../../plugins/bootbox/bootbox.js"></script>
</body>
</html>
