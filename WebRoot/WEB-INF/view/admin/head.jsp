<%@ page pageEncoding="utf-8" session="false"%>
<%
  String contextPath = request.getContextPath();
  String webPath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
  request.setAttribute("webPath",webPath);
%>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="shortcut icon" href="<%=webPath%>favicon.ico" type="image/x-icon" />
<link rel="icon" sizes="any" href="<%=webPath%>img/logo/logo.svg" />
<link rel="stylesheet" href="<%=webPath%>plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=webPath%>plugins/adminlte/css/AdminLTE.min.css">
<link rel="stylesheet" href="<%=webPath%>plugins/adminlte/css/skins/skin-blue-light.min.css">
<link rel="stylesheet" href="<%=webPath%>plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<%=webPath%>plugins/toastr/toastr.css">
<script src="<%=webPath%>plugins/jquery/jquery-2.2.3.min.js"></script>
<script src="<%=webPath%>plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<%=webPath%>plugins/adminlte/js/app.min.js"></script>
<script src="<%=webPath%>plugins/toastr/toastr.min.js"></script>
<script type="text/javascript">
  /**
   * 注销
   */
  function logout() {
    $.ajax({
      type: 'post',
      url: '<%=webPath%>v1/admin/logout',
      success:function(){
        location.href="<%=webPath%>";
      },error:function(){
        location.href="<%=webPath%>";
      }
    });
  }
</script>
