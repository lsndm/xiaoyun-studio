<%@ page pageEncoding="utf-8" session="false" %>
<header class="main-header">
  <%-- 左上角的logo--%>
  <a href="#" class="logo">
    <span class="logo-mini">小云</span>
    <span class="logo-lg">小云工作室</span>
  </a>
  <nav class="navbar navbar-static-top" role="navigation">
    <%-- 汉堡按钮，点击后可以折叠和展开左边的菜单栏--%>
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
    </a>
    <%-- 右上角的一些按钮 --%>
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <li><a>小云</a></li>
        <li>
          <a href="javascript:logout()">注销</a>
        </li>
      </ul>
    </div>
  </nav>
</header>
<%-- 左边的菜单栏 --%>
<aside class="main-sidebar">
  <section class="sidebar">
    <ul class="sidebar-menu">
      <li class="header"></li>
      <li id="home"><a href="${requestScope.webPath}admin/index"><i class="fa fa-home"></i><span>首页</span></a></li>
      <li class="treeview">
        <a href="#"><i class="fa fa-lock"></i><span>安全管理</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
          <li id="authManage"><a href="${requestScope.webPath}admin/securityManage/authManage">权限管理</a></li>
          <li id="roleManage"><a href="${requestScope.webPath}admin/securityManage/roleManage">角色管理</a></li>
          <li id="roleAuth"><a href="${requestScope.webPath}admin/securityManage/roleAuth">角色权限</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#"><i class="fa fa-comments"></i><span>消息管理</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
          <li id="feedbackManage"><a href="${requestScope.webPath}admin/messageManage/feedbackManage">反馈管理</a></li>
        </ul>
      </li>
    </ul>
  </section>
</aside>
<%-- 使菜单被选中 --%>
<script type="text/javascript">
  var menuId = '${param.menuId}';
  if ($(".sidebar-menu>li").is("#" + menuId)) { //一级菜单
    $('#' + menuId).addClass("active");
  } else {  //二级菜单
    $('#' + menuId).parent().parent().addClass("active");
  }
</script>