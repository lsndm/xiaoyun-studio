<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <%@ include file="../head.jsp" %>
  <title>反馈管理-小云工作室</title>
  <link rel="stylesheet" href="../../plugins/bootstrap-table/bootstrap-table.min.css">
  <link rel="stylesheet" href="../../css/admin/base.css">
  <script type="text/javascript">
    function deleteData() {
      toastr.options = {"positionClass": "toast-top-center"};
      var rows = $('#table').bootstrapTable('getSelections');
      if (rows.length == 0) {
        toastr.info("请至少选择一行！");
        return;
      }
      var permissionId = $.map(rows, function (item) {
        return item.feedbackId;
      });
      bootbox.confirm({
        title: "系统提示",
        message: "确定要删除吗？",
        buttons: {
          confirm: {
            label: '<i class="fa fa-check"></i> 确定'
          },
          cancel: {
            label: '<i class="fa fa-times"></i> 取消'
          }
        },
        callback: function (result) {
          if(result){
            $.ajax({
              type: 'delete',
              url: '../../v1/feedback',
              data: {feedbackId: JSON.stringify(permissionId)},
              success: function (data) {
                $('#table').bootstrapTable('refresh');
                toastr.success("删除成功！");
              }, error: function (data) {
                if (data.responseJSON && data.responseJSON.error) {
                  toastr.error(data.responseJSON.error.message);
                } else {
                  toastr.error("系统错误，请稍后重试！");
                }
              }
            });
          }
        }
      });
    }
  </script>
</head>
<body class="hold-transition skin-blue-light sidebar-mini">
<div class="wrapper">
  <jsp:include page="../header.jsp">
    <jsp:param name="menuId" value="feedbackManage"/>
  </jsp:include>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>反馈管理</h1>
      <ol class="breadcrumb">
        <li><a href="../index"><i class="fa fa-home"></i></a></li>
        <li><a href="#">消息管理</a></li>
        <li class="active">反馈管理</li>
      </ol>
    </section>
    <section class="content">
      <div id="toolbar">
        <button class="btn btn-default" onclick="deleteData()"><i class="fa fa-remove"></i> 删除</button>
      </div>
      <table id="table" data-toggle="table" data-url="../../v1/feedback?sort=createDate desc" data-toolbar="#toolbar">
        <thead>
        <tr>
          <th data-checkbox="true"></th>
          <th data-field="feedbackId" data-visible="false">主键</th>
          <th data-field="text" data-searchable="true">内容</th>
          <th data-field="title" data-searchable="true">联系方式</th>
          <th data-field="createDate">创建时间</th>
        </tr>
        </thead>
      </table>
    </section>
  </div>
</div>
<script src="../../plugins/bootstrap-table/bootstrap-table.min.js"></script>
<script src="../../plugins/bootstrap-table/bootstrap-table-xiaoyun.js"></script>
<script src="../../plugins/bootbox/bootbox.js"></script>
</body>
</html>
