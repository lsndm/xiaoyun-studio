<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <%@ include file="head.jsp" %>
  <title>首页-小云工作室</title>
</head>
<body class="hold-transition skin-blue-light sidebar-mini">
<div class="wrapper">
  <jsp:include page="header.jsp">
    <jsp:param name="menuId" value="home"/>
  </jsp:include>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>首页</h1>
    </section>
    <section class="content">

    </section>
  </div>
</div>
</body>
</html>