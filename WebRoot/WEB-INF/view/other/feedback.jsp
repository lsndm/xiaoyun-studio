<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <%@ include file="../header.jsp" %>
  <script type="text/javascript">
    function createFeedback() {
      var text = $("#text").val();
      var messageSpan = $("#message");
      messageSpan.empty();
      messageSpan.removeClass('alert-warning alert-success alert-danger');
      if (text == null || text.length < 1) {
        messageSpan.append("留言至少要有一个字!");
        messageSpan.addClass("alert-warning");
      } else {
        $.ajax({
          type: 'put',
          url: '../v1/feedback',
          data: {
            format: "json",
            text: text,
            title: '网友'
          }}).done(function () {
            messageSpan.append("提交成功，感谢留言!");
            messageSpan.addClass("alert-success");
            $("#text").val("");
          }).fail(function () {
            messageSpan.append("提交失败,请稍后重试!");
            messageSpan.addClass("alert-danger");
          });
      }
    }
  </script>
  <title>留言-小云工作室</title>
</head>
<body>
<div class="container">
  <div class="row">
    <div class="col-sm-6">
      <h1 class="text-center">留言</h1>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <form role="form">
        <div class="form-group">
          <textarea class="form-control" id="text" maxlength="140" style="height: 10em" placeholder="请输入留言"></textarea>
        </div>
        <div class="form-group">
          <button type="button" class="btn btn-default" onclick="createFeedback()">提交</button>
          <span>&nbsp;&nbsp;&nbsp;联系我请使用邮箱:<a href="mailto:xiaoyun-studio@foxmail.com">xiaoyun-studio@foxmail.com</a></span>
        </div>
        <div class="form-group">
          <p id="message" class="alert"></p>
        </div>
      </form>
    </div>
  </div>
</div>
</body>
</html>