<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <%@ include file="../header.jsp" %>
  <script type="text/javascript">
    function login() {
      var password = $("#password").val();
      var name = $("#name").val();
      var messageSpan = $("#message");
      messageSpan.empty();
      messageSpan.removeClass('alert-warning alert-success alert-danger');
      if (name == null || name.length < 1) {
        messageSpan.append("请输入用户名!");
        messageSpan.addClass("alert-warning");
      } else if (password == null || password.length < 1) {
        messageSpan.append("请输入密码!");
        messageSpan.addClass("alert-warning");
      } else {
        $.ajax({
          type: 'post',
          url: '../v1/admin/login',
          data: {
            format: "json",
            password: password,
            name: name
          }
        }).done(function (data) {
          location.href = "../admin/index";
        }).fail(function (error) {
          var errorCode = "INTERNAL_SERVER_ERROR";
          var message = "登陆失败,请稍后重试!";
          if (error.responseJSON && error.responseJSON.error) {
            errorCode = error.responseJSON.error.code;
          }
          switch (errorCode) {
            case "UNKNOWN_ACCOUNT":
              message = "用户不存在!";
              break;
            case "INCORRECT_PASSWORD":
              message = "密码错误!";
              break;
            case "DISABLED_ACCOUNT":
              message = "账户不可用!";
              break;
          }
          messageSpan.append(message);
          messageSpan.addClass("alert-danger");
        });
      }
    }
    /**
     * 注销
     */
    function logout() {
      $.ajax({
        type: 'post',
        url: '../v1/admin/logout'
      }).done(function (data) {
        location.reload(true);
      }).fail(function (error) {
        location.reload(true);
      });
    }
  </script>
  <title>管理员登陆-小云工作室</title>
</head>
<body>
<div class="container">
  <div class="row">
    <div class="col-sm-6">
      &nbsp;&nbsp;
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <shiro:guest>
        <form role="form">
          <div class="form-group">
            <label for="name">用户名:</label>
            <input type="text" class="form-control" id="name" maxlength="50" autofocus="autofocus" placeholder="请输入用户名"/>
          </div>
          <div class="form-group">
            <label for="password">密码:</label>
            <input type="password" class="form-control" id="password" maxlength="30" placeholder="请输入密码"/>
          </div>
          <div class="form-group">
            <button type="button" class="btn btn-default" onclick="login()">登陆</button>
          </div>
          <div class="form-group">
            <p id="message" class="alert"></p>
          </div>
        </form>
      </shiro:guest>
      <shiro:user>
        您已经登陆！&nbsp;&nbsp;<a href="javascript:logout()">注销</a>&nbsp;&nbsp;<a href="../">返回首页</a>
      </shiro:user>
    </div>
  </div>
</div>
</body>
</html>