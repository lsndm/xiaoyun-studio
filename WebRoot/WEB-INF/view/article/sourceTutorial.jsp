<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <%@ include file="../header.jsp" %>
  <title>源码使用教程</title>
  <style type="text/css">
    p {
      text-indent: 2em;
    }

    img {
      margin: auto;
    }

    small {
      float: right;
    }
  </style>
</head>
<body>
<div class="container">
  <div class="row">
    <h1 class="text-center">源码使用教程</h1>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <section>
        <h3 id="title_1">一、概述</h3>
        <p>
          “小云工作室”的源码托管在<a href="http://git.oschina.net/xiaoyun_studio/xiaoyun-studio"
                            target="_blank">码云(http://git.oschina.net/xiaoyun_studio/xiaoyun-studio)</a>上， 感兴趣的网友可以去查看、修改源码或者提出意见。
        </p>
        <p>本教程会讲解如何使用Eclipse获得项目源码，并在Tomcat中运行项目。</p>
      </section>
      <section>
        <h3 id="title_2">二、获得源码</h3>
        <p>1.在Eclipse的项目资源管理器的空白区域右键，选择导入项目，打开如下图所示的界面，在该界面中选择导入项目的方式，
          支持GIT和SVN两种方式，本教程以GIT为例。</p>
        <p><img src="../img/article/import-1.png" class="img-responsive"></p>
        <p>2.图中的第一个选项表示从本地的GIT仓库中导入项目，第二个选项表示从网络上下载源码。如果已经用TortoiseGit之类的软件将源码下载到了本地，
          就选择第一个选择，否则选择第二个选项。</p>
        <p><img src="../img/article/import-2.png" class="img-responsive"></p>
        <p>3.如果在上一步中选择第二个选项，就可以看到如下图所示的界面。URI填写:https://git.oschina.net/xiaoyun_studio/xiaoyun-studio.git,
          其它选项不需要填写。如果使用SVN的方式导入项目，SVN地址为:svn://git.oschina.net/xiaoyun_studio/xiaoyun-studio</p>
        <p><img src="../img/article/import-3.png" class="img-responsive"></p>
        <p>4.这里选择项目分支</p>
        <p><img src="../img/article/import-4.png" class="img-responsive"></p>
        <p>5.这里选择将源码下载到什么目录中</p>
        <p><img src="../img/article/import-5.png" class="img-responsive"></p>
        <p>6.这里选择如何生成项目，必须选择第三个选项</p>
        <p><img src="../img/article/import-6.png" class="img-responsive"></p>
        <p>7.如下图所示，项目源码已经成功导入Eclipse</p>
        <p><img src="../img/article/import-7.png" class="img-responsive"></p>
      </section>
      <section>
        <h3 id="title_3">三、编译代码
          <small><a href="#title_1">返回顶部</a></small>
        </h3>
        <p>1.由于本项目基于Maven构建，所以要将项目转换为Maven项目，方法如下图所示:</p>
        <p><img src="../img/article/compile-1.png" class="img-responsive"></p>
        <p>2.转换为Maven项目后的目录结构如下图所示:</p>
        <p><img src="../img/article/compile-2.png" class="img-responsive"></p>
        <p>3.本项目是基于JDK1.8开发的，如果你的JDK版本低于1.8的话，会出现编译错误</p>
      </section>
      <section>
        <h3 id="title_4">四、部署、运行
          <small><a href="#title_1">返回顶部</a></small>
        </h3>
        <p>1.在Eclipse的Servers视图中可以将项目部署到Tomcat中</p>
        <p><img src="../img/article/deploy-1.png" class="img-responsive"></p>
        <p>2.在这个界面中选择要部署的项目</p>
        <p><img src="../img/article/deploy-2.png" class="img-responsive"></p>
        <p>3.运行Tomcat，如果控制台输出如下图所示的信息，并且没有报错，则启动成功</p>
        <p><img src="../img/article/deploy-3.png" class="img-responsive"></p>
        <p>4.在浏览器地址栏输入地址“http://localhost:8080/xiaoyun-studio”就可以看到项目首页</p>
      </section>
      <div style="height:20px"></div>
    </div>
  </div>
</div>
</body>
</html>