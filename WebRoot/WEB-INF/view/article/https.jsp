<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" session="false" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <%@ include file="../header.jsp" %>
  <title>免费HTTPS</title>
  <style type="text/css">
   p{
     text-indent: 1em;
   }
  </style>
</head>
<body>
<div class="container">
  <div class="row">
    <h1 class="text-center">免费HTTPS</h1>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <section>
        <p>Let's Encrypt是一个免费SSL证书发行项目，发行的证书已经获得主流浏览器的支持</p>
        <p>本文讲解如何使用Let's Encrypt获得免费SSL证书，配置apache的SSL功能，并将请求转发到tomcat</p>
        <p>大致思路如下：</p>
        <ul>
          <li>1.使用Let's Encrypt获得SSL证书</li>
          <li>2.开启apache的SSL功能，用户请求首先到apache，使用http协议或者https协议</li>
          <li>3.apache将请求转发到tomcat上，使用http协议或者ajp协议</li>
        </ul>
        <p>这样做的原因有以下几点：</p>
        <ul>
          <li>1.我的网站是java编写的，需要一个类似于tomcat的servlet容器</li>
          <li>2.虽然tomcat也支持ssl，但Let's Encrypt原生支持apache，所以在apache上配置ssl比较简单</li>
          <li>3.apache处理静态资源的能力 比tomcat好</li>
          <li>4.可以配置多个tomcat实现负载均衡，可以在不终止服务的情况下重启tomcat</li>
        </ul>
      </section>
      <section>
        <h3>一、安装Let's Encrypt</h3>
        <p> Let's Encrypt的项目主页是 https://github.com/certbot/certbot ，可以使用git下载，也可以到主页直接下载ZIP压缩包</p>
        <p>进入文件夹中，执行如下命令，之后Let's Encrypt会安装一些软件</p>
        <pre>./letsencrypt-auto --help</pre>
      </section>
      <section>
        <h3>二、安装apache</h3>
        <p>在centos7中安装apache的命令</p>
        <pre>yum install httpd</pre>
        <p>在centos7中安装apache的ssl模块</p>
        <pre>yum install mod_ssl</pre>
        <p>在ubuntu中安装apache</p>
        <pre>sudo apt-get install apache2</pre>
      </section>
      <section>
        <h3>三、安装tomcat</h3>
        <p>到官网下载一个tomcat，解压就能用，当然先要装jre</p>
      </section>
      <section>
        <h3>四、配置apache</h3>
        <p>我安装的版本是2.4.6，这个版本的配置和其它版本不太一样</p>
        <p>在centos7中apache配置文件的路径是/etc/httpd/conf/httpd.conf , ssl模块的配置文件的路径是/etc/httpd/conf.d/ssl.conf</p>
        <p>在ubuntu中apache配置文件的路径是/etc/apache2/apache2.conf</p>
        <p>修改httpd.conf文件</p>
        <p>添加如下代码，功能是将http协议的请求自动跳转到https协议</p>
<pre>
RewriteEngine on
RewriteCond %{SERVER_PORT} 80
RewriteRule ^(.*)$ https://%{SERVER_NAME}%{REQUEST_URI} [R=301,L]
</pre>
        <p>修改ssl.conf文件</p>
        <p>将Listen 443 https 删掉，不然会和后边的虚拟主机配置冲突</p>
        <p>将&lt;VirtualHost *:443&gt;标签中#ServerName www.example.com:443 前边的#去掉，域名改为自己的域名，Let's
          Encrypt生成的证书就是绑定这个域名的，我试过改成ip后，Let's Encrypt不支持</p>
        <p>在&lt;VirtualHost *:443&gt;标签中添加以下代码， 功能是将所有请求转发到tomcat服务器 ，这里用的是ajp协议，如果想用http协议，将ajp改为http，8009改为8080</p>
<pre>
ProxyVia On
ProxyRequests Off
ProxyPass / ajp://127.0.0.1:8009/
ProxyPassReverse / ajp://127.0.0.1:8009/
&lt;Proxy *&gt;
  Require all granted
&lt;/Proxy&gt;
&lt;Location /&gt;
  Require all granted
&lt;/Location&gt;
</pre>
        <p>配置文件改好后，使用httpd -t命令检查配置文件是否有错误，没错误的话，就可以使用service httpd start启动服务</p>
        <p>启动服务的时候还出了一个错误：AH00558: httpd: Could not reliably determine the server's fully qualified domain name,
          将httpd.conf中#ServerName localhost:80的#去掉，localhost改为实际的ip就好了</p>
      </section>
      <section>
        <h3>五、生成证书</h3>
        <p>Let's Encrypt支持3种认证方式</p>
        <p>--apache Use the Apache plugin for authentication &amp; installation</p>
        <p>--standalone Run a standalone webserver for authentication</p>
        <p>--webroot Place files in a server's webroot folder for authentication</p>
        <p>这里使用apache的认证方式，命令如下，</p>
        <pre>./letsencrypt-auto --apache --apache-le-vhost-ext /etc/httpd/conf.d/ssl.conf --register-unsafely-without-email</pre>
        <p>--apache-le-vhost-ext参数指定配置虚拟主机的配置文件；--register-unsafely-without-email参数是忽略邮箱，如果没有这个参数，
          Let's Encrypt会要求输入邮箱，我输入foxmail邮箱，却提示无效，不知道为什么
          证书生成后会放在/etc/letsencrypt/live/www.example.com/目录中,Let's Encrypt会自动修改ssl.conf文件，关联证书</p>
      </section>
      <section>
        <h3>六、运行apache、tomcat</h3>
        <p>运行tomcat并输出日志</p>
<pre>
cd /etc/tomcat
sh bin/startup.sh;tail -f logs/catalina.out
</pre>
        <p>运行apache</p>
        <pre>service httpd start</pre>
      </section>
    </div>
  </div>
</div>
</body>
</html>