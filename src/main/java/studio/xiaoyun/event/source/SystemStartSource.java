package studio.xiaoyun.event.source;

import java.util.Date;

/**
 * 封装系统启动时的一些信息
 */
public class SystemStartSource {
    /**
     * 系统启动时间
     */
    private Date systemStartDate;
    public Date getSystemStartDate() {
        return systemStartDate;
    }

    public void setSystemStartDate(Date systemStartDate) {
        this.systemStartDate = systemStartDate;
    }

}
