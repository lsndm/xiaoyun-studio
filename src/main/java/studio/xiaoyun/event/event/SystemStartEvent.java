package studio.xiaoyun.event.event;

import reactor.bus.Event;
import studio.xiaoyun.event.source.SystemStartSource;

/**
 * 系统启动时触发的事件
 */
public class SystemStartEvent extends Event<SystemStartSource>{
    public static final String EVENT_NAME = "system_start";
    public SystemStartEvent(SystemStartSource source) {
        super(source);
    }
}
