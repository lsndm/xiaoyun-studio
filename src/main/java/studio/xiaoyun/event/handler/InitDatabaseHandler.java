package studio.xiaoyun.event.handler;

import org.springframework.stereotype.Component;
import reactor.bus.EventBus;
import reactor.spring.context.annotation.Consumer;
import reactor.spring.context.annotation.Selector;
import studio.xiaoyun.event.event.SystemStartEvent;

import javax.annotation.Resource;

/**
 * 在系统启动时初始化数据库
 */
@Consumer
@Component
public class InitDatabaseHandler {
    @Resource
    private EventBus eventBus;

    @Selector(SystemStartEvent.EVENT_NAME)
    public void initDatabase(SystemStartEvent event){
    }

}
