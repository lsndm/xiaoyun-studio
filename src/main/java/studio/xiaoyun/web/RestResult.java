package studio.xiaoyun.web;

import org.springframework.web.servlet.ModelAndView;
import studio.xiaoyun.core.exception.InvalidParameterException;
import studio.xiaoyun.web.dto.AbstractDTO;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Rest控制器的返回值都应该用这个类
 */
public class RestResult extends ModelAndView {
	public static final String KEY_TOTAL = "_total";
	public static final String KEY_ROWS = "_rows";

	/**
	 * 构造函数
	 * @param total 数量
	 * @param row 一行数据，可以为null
	 */
	public RestResult(long total, AbstractDTO row) {
		if (total < 0) {
			throw new InvalidParameterException("total参数不能小于0");
		}
		this.getModel().put(KEY_TOTAL, total);
		if(row != null){
			this.getModel().put(KEY_ROWS, Collections.singletonList(row));
		}
	}

	/**
	 * 构造函数
	 * @param total 数量
	 * @param rows 多行数据，可以为null
	 */
	@SuppressWarnings("rawtypes")
	public RestResult(long total, List rows) {
		if (total < 0) {
			throw new InvalidParameterException("total参数不能小于0");
		}
		this.getModel().put(KEY_TOTAL, total);
		if(rows != null){
			this.getModel().put(KEY_ROWS, rows);
		}
	}

	/**
	 * 构造函数
	 * @param total 数量
	 * @param row 一行数据，可以为null
	 */
	@SuppressWarnings("rawtypes")
	public RestResult(long total, Map row) {
		if (total < 0) {
			throw new InvalidParameterException("total参数不能小于0");
		}
		this.getModel().put(KEY_TOTAL, total);
		if(row != null){
			this.getModel().put(KEY_ROWS, Collections.singletonList(row));
		}
	}
	
}
