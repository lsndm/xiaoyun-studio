package studio.xiaoyun.web;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.util.Collections;
import java.util.List;

@XStreamAlias("response")
public class HttpResponse {

	/**
	 * 总数
	 */
	private Long total;
	/**
	 * 行
	 */
	@SuppressWarnings("rawtypes")
	private List rows;
	/**
	 * 异常信息
	 */
	private HttpError error;

	public Long getTotal() {
		return total==null?0L:total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public List getRows() {
		return rows == null ? Collections.emptyList() : rows;
	}

	@SuppressWarnings("rawtypes")
	public void setRows(List rows) {
		this.rows = rows;
	}
	
	/**
	 * 取得封装的异常信息
	 * @return 异常信息，可能为null
	 */
	public HttpError getError() {
		return error;
	}

	public void setError(HttpError error) {
		this.error = error;
	}
}
