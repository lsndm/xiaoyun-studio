package studio.xiaoyun.web;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

/**
 * 封装http响应中的异常信息
 */
@XStreamAlias("error")
public class HttpError {
	/**
	 * 异常信息
	 */
	private String message;
	/**
	 * 异常堆栈
	 */
	private List<String> stack;
	/**
	 * 错误码
	 */
	private ErrorCode code;

	public ErrorCode getCode() {
		return code;
	}

	public void setCode(ErrorCode code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}
	
	public List<String> getStack() {
		return stack;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public void setStack(Exception exception) {
		stack = new LinkedList<>();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		PrintWriter writer = new PrintWriter(out);
		exception.printStackTrace(writer);
		writer.close();
		String[] stackTraces = out.toString().split(System.getProperty("line.separator"));
        for(String stackTrace:stackTraces){
			stack.add(stackTrace.replaceAll("\t","    "));
		}
	}

}
