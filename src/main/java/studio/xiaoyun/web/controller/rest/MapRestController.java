package studio.xiaoyun.web.controller.rest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import studio.xiaoyun.web.RestResult;
import studio.xiaoyun.web.dto.LinkDTO;
import studio.xiaoyun.web.dto.NodeDTO;

@RestController
@RequestMapping("/v1/map")
public class MapRestController {
	
	/**
	 * 
	 * @return 首页显示的网络图数据
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	public RestResult getMap(){
		Map<String,List<?>> map = new HashMap<>();
		List<NodeDTO> nodes = new LinkedList<>();
		NodeDTO node = new NodeDTO();
		node.setId("1");
		node.setTitle("小云");
		node.setUrl("#");
		nodes.add(node);
		node = new NodeDTO();
		node.setId("2");
		node.setTitle("应用");
		node.setUrl("app/index.html");
		nodes.add(node);
		node = new NodeDTO();
		node.setId("3");
		node.setTitle("介绍");
		node.setUrl("#");
		nodes.add(node);
		node = new NodeDTO();
		node.setId("4");
		node.setTitle("文档");
		node.setUrl("article/index.html");
		nodes.add(node);
		node = new NodeDTO();
		node.setId("5");
		node.setTitle("源码");
		node.setUrl("article/sourceTutorial.html");
		nodes.add(node);
		node = new NodeDTO();
		node.setId("6");
		node.setTitle("留言");
		node.setUrl("other/feedback.html");
		nodes.add(node);
		node = new NodeDTO();
		node.setId("9");
		node.setTitle("上传文件");
		node.setUrl("app/uploadFile.html");
		nodes.add(node);
		node = new NodeDTO();
		node.setId("10");
		node.setTitle("文件预览");
		node.setUrl("app/preview.html");
		nodes.add(node);
		node = new NodeDTO();
		node.setId("11");
		node.setTitle("免费HTTPS");
		node.setUrl("article/https.html");
		nodes.add(node);
		map.put("nodes", nodes);
		
		List<LinkDTO> links = new LinkedList<>();
		LinkDTO link = new LinkDTO();
		link.setSource("1");
		link.setTarget("2");
		links.add(link);
		link = new LinkDTO();
		link.setSource("1");
		link.setTarget("3");
		links.add(link);
		link = new LinkDTO();
		link.setSource("1");
		link.setTarget("4");
		links.add(link);
		link = new LinkDTO();
		link.setSource("1");
		link.setTarget("5");
		links.add(link);
		link = new LinkDTO();
		link.setSource("1");
		link.setTarget("6");
		links.add(link);
		link = new LinkDTO();
		link.setSource("2");
		link.setTarget("9");
		links.add(link);
		link = new LinkDTO();
		link.setSource("2");
		link.setTarget("10");
		links.add(link);
		link = new LinkDTO();
		link.setSource("4");
		link.setTarget("11");
		links.add(link);
		map.put("links", links);
		return new RestResult(1L,map);
	}

}
