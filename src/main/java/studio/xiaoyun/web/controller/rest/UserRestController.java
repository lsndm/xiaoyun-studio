package studio.xiaoyun.web.controller.rest;

import org.apache.shiro.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import studio.xiaoyun.core.exception.InvalidParameterException;
import studio.xiaoyun.core.constant.Permission;
import studio.xiaoyun.core.dao.UserDao;
import studio.xiaoyun.core.pojo.UserDO;
import studio.xiaoyun.core.service.UserService;
import studio.xiaoyun.security.annotation.RequirePermission;
import studio.xiaoyun.web.RestResult;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 用户相关
 */
@RestController
@RequestMapping("/v1/user")
public class UserRestController {
    @Resource
    private UserDao userDao;
    @Resource
    private UserService userService;

    @RequirePermission(Permission.USER_CREATE)
    @RequestMapping(value = "",method = RequestMethod.PUT)
    public RestResult saveUser(String email, String password) {
        if(!StringUtils.hasText(email) || !StringUtils.hasText(password)){
            throw new InvalidParameterException("邮箱和密码不能为空!");
        }
        String userID = userService.saveUser(email,password);
        UserDO user = userDao.getById(userID);
        Map<String,String> map = new HashMap<>();
        map.put("name",user.getName()+" ");
        return new RestResult(1,map);
    }
}
