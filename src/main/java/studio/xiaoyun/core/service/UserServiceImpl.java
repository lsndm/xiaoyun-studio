package studio.xiaoyun.core.service;

import org.springframework.stereotype.Service;
import studio.xiaoyun.core.dao.UserDao;
import studio.xiaoyun.core.constant.UserStatus;
import studio.xiaoyun.core.pojo.UserDO;

import javax.annotation.Resource;
import java.time.LocalDateTime;

@Service("userService")
public class UserServiceImpl implements studio.xiaoyun.core.service.UserService {
    @Resource
    private UserDao userDao;

    @Override
    public String saveUser(String email, String password) {
        UserDO user = new UserDO();
        user.setPassword(password);
        user.setEmail(email);
        user.setCreateDate(LocalDateTime.now());
        user.setStatus(UserStatus.NORMAL);
        String userID = userDao.save(user);
        user.setName("网友"+userID);
        return userID;
    }
}
