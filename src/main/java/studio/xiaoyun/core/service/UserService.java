package studio.xiaoyun.core.service;

/**
 * 用户业务类
 */
public interface UserService {
    /**
     * 创建用户
     * @param email 邮箱
     * @param password 密码
     * @return 用户id
     */
    String saveUser(String email, String password);
}
