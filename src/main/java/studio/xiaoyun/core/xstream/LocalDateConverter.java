package studio.xiaoyun.core.xstream;

import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * 实现LocalDate和字符串之间的转换
 */
public class LocalDateConverter extends AbstractSingleValueConverter {
    private String pattern;

    /**
     * @param pattern 字符串的模式
     */
    public LocalDateConverter(String pattern) {
        this.pattern = pattern;
    }

    @Override
    public boolean canConvert(Class type) {
        return type == LocalDate.class;
    }

    @Override
    public Object fromString(String str) {
        return LocalDate.parse(str, DateTimeFormatter.ofPattern(pattern));
    }

    @Override
    public String toString(Object obj) {
        return ((LocalDate)obj).format(DateTimeFormatter.ofPattern(pattern));
    }
}
