package studio.xiaoyun.core.xstream;

import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 实现LocalDateTime和字符串之间的转换
 */
public class LocalDateTimeConverter extends AbstractSingleValueConverter {
    private String pattern;

    /**
     *
     * @param pattern 字符串的模式
     */
    public LocalDateTimeConverter(String pattern) {
        this.pattern = pattern;
    }

    @Override
    public boolean canConvert(Class type) {
        return type == LocalDateTime.class;
    }

    @Override
    public Object fromString(String str) {
        return LocalDateTime.parse(str,DateTimeFormatter.ofPattern(pattern));
    }

    @Override
    public String toString(Object obj) {
        return ((LocalDateTime)obj).format(DateTimeFormatter.ofPattern(pattern));
    }
}
