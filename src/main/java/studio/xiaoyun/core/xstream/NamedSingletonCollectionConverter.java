package studio.xiaoyun.core.xstream;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.converters.collections.SingletonCollectionConverter;
import com.thoughtworks.xstream.core.util.HierarchicalStreams;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.mapper.Mapper;

public class NamedSingletonCollectionConverter extends SingletonCollectionConverter {

    private final Mapper mapper;
    private final String name;

    public NamedSingletonCollectionConverter(Mapper mapper,String name) {
        super(mapper);
        this.mapper = mapper;
        this.name = name;
    }

    @Override
    protected void writeItem(Object item, MarshallingContext context, HierarchicalStreamWriter writer) {
        writer.startNode(name);
        String attributeName = mapper.aliasForSystemAttribute("class");
        writer.addAttribute(attributeName, mapper.serializedClass(item.getClass()));
        context.convertAnother(item);
        writer.endNode();
    }

    @Override
    protected Object readItem(HierarchicalStreamReader reader, UnmarshallingContext context, Object current) {
        String className = HierarchicalStreams.readClassAttribute(reader, mapper);
        Class itemType = mapper.realClass(className);
        return context.convertAnother(null, itemType);
    }
}
