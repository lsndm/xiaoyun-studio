package studio.xiaoyun.core.xstream;

import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * 实现LocalTime和字符串之间的转换
 */
public class LocalTimeConverter extends AbstractSingleValueConverter {
    private String pattern;

    /**
     *
     * @param pattern 字符串的模式
     */
    public LocalTimeConverter(String pattern) {
        this.pattern = pattern;
    }

    @Override
    public boolean canConvert(Class type) {
        return type == LocalTime.class;
    }

    @Override
    public Object fromString(String str) {
        return LocalTime.parse(str, DateTimeFormatter.ofPattern(pattern));
    }

    @Override
    public String toString(Object obj) {
        return ((LocalTime)obj).format(DateTimeFormatter.ofPattern(pattern));
    }
}
