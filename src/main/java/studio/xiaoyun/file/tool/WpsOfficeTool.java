package studio.xiaoyun.file.tool;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.ComThread;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

import studio.xiaoyun.file.converter.IPdfConverter;
import studio.xiaoyun.core.exception.XyException;

/**
 * 使用wps office作为转换器将文件转换为其它格式
 */
public class WpsOfficeTool implements IPdfConverter {
    private Logger log = LoggerFactory.getLogger(WpsOfficeTool.class);
    private List<String> supportedExtensionsForPDFConverter;
    
    @Override
    public void docToPdf(String sourceFilePath, String targetFilePath) {
        ActiveXComponent pptActiveXComponent = null;
        ActiveXComponent workbook = null;
        try {
            ComThread.InitSTA();//初始化COM线程
            pptActiveXComponent = new ActiveXComponent("KWPS.Application");//初始化exe程序
            Variant[] openParams = new Variant[]{
                    new Variant(sourceFilePath),
                    new Variant(true),
                    new Variant(true)//readOnley
            };
            workbook = pptActiveXComponent.invokeGetComponent("Documents").invokeGetComponent
                    ("Open", openParams);
            workbook.invoke("SaveAs", new Variant(targetFilePath), new Variant(17));
        }catch (Exception e) {
            log.error("转换文件失败：" + sourceFilePath, e);
            throw new XyException("文件转换失败",e);
        } finally {
            if (workbook != null) {
                workbook.invoke("Close");
                workbook.safeRelease();
            }
            if (pptActiveXComponent != null) {
                pptActiveXComponent.invoke("Quit");
                pptActiveXComponent.safeRelease();
            }
            ComThread.Release();
        }
    }

    @Override
    public void docxToPdf(String sourceFilePath, String targetFilePath) {
        docToPdf( sourceFilePath,  targetFilePath);
    }

    @Override
    public void xlsToPdf(String sourceFilePath, String targetFilePath) {
        ActiveXComponent et = null;
        Dispatch workbooks;
        Dispatch workbook = null;
        ComThread.InitSTA();//初始化COM线程
        try {
            et = new ActiveXComponent("KET.Application");//初始化et.exe程序
            et.setProperty("Visible", new Variant(false));
            workbooks = et.getProperty("Workbooks").toDispatch();
            workbook = Dispatch.invoke(workbooks, "Open", Dispatch.Method, new Object[]{sourceFilePath, 0, true}, new int[1]).toDispatch();
            Dispatch.call(workbook, "ExportAsFixedFormat", 0, targetFilePath);
        } catch (Exception e) {
            log.error("转换文件失败：" + sourceFilePath, e);
            throw new XyException("文件转换失败",e);
        }finally {
            if (workbook != null) {
                Dispatch.call(workbook, "Close");
                workbook.safeRelease();
            }
            if (et != null) {
                et.invoke("Quit");
                et.safeRelease();
            }
            ComThread.Release();
        }
    }

    @Override
    public void xlsxToPdf(String sourceFilePath, String targetFilePath) {
        xlsToPdf( sourceFilePath,  targetFilePath);
    }

    @Override
    public void pptToPdf(String sourceFilePath, String targetFilePath) {
        ActiveXComponent pptActiveXComponent = null;
        ActiveXComponent workbook = null;
        try {
            ComThread.InitSTA();//初始化COM线程
            pptActiveXComponent = new ActiveXComponent("KWPP.Application");//初始化exe程序
            workbook = pptActiveXComponent.invokeGetComponent("Presentations").invokeGetComponent
                    ("Open", new Variant(sourceFilePath), new Variant(true));
            workbook.invoke("SaveAs", new Variant(targetFilePath), new Variant(32));
        }catch (Exception e) {
            log.error("转换文件失败：" + sourceFilePath, e);
            throw new XyException("文件转换失败",e);
        } finally {
            if (workbook != null) {
                workbook.invoke("Close");
                workbook.safeRelease();
            }
            if (pptActiveXComponent != null) {
                pptActiveXComponent.invoke("Quit");
                pptActiveXComponent.safeRelease();
            }
            ComThread.Release();
        }
    }

    @Override
    public void pptxToPdf(String sourceFilePath, String targetFilePath) {
        pptToPdf( sourceFilePath,  targetFilePath);
    }
    
    @Override
	public List<String> getSupportedExtensionsForPdf() {
		return supportedExtensionsForPDFConverter==null?Collections.emptyList():supportedExtensionsForPDFConverter;
	}

	@Override
	public void setSupportedExtensionsForPdf(List<String> supportedExtensionsForPDFConverter) {
		this.supportedExtensionsForPDFConverter = supportedExtensionsForPDFConverter;
	}

}
