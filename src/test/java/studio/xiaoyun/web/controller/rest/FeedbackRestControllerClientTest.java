package studio.xiaoyun.web.controller.rest;

import com.jayway.jsonpath.JsonPath;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import studio.xiaoyun.core.dao.FeedbackDao;
import studio.xiaoyun.core.dao.TestUtil;
import studio.xiaoyun.core.pojo.FeedbackDO;
import studio.xiaoyun.web.PublicParameter;

import javax.annotation.Resource;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextHierarchy({  
    @ContextConfiguration(name = "parent", locations = "classpath:config/spring-context-test.xml"),  
    @ContextConfiguration(name = "child", locations = "classpath:config/spring-mvc-test.xml")  
}) 
public class FeedbackRestControllerClientTest {
	@Resource
	private WebApplicationContext wac;
	private MockMvc mockMvc;
	@Resource
	private FeedbackDao feedbackDao;
	@Resource
	private TestUtil testUtil;

	@Before
	public void setup() throws Exception {
		testUtil.loginForAdmin();
	    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}
	
	@Test
	public void testCreateFeedback() throws Exception {
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.put("/v1/feedback");
		request.param("title", "aaa");
		request.param("text", "bbb");
		request.param(PublicParameter.FORMAT.value(), "json");
		ResultActions action = mockMvc.perform(request);
		action.andExpect(status().is2xxSuccessful());
		String content = action.andReturn().getResponse().getContentAsString();
		String ID = JsonPath.read(content,"$.rows[0].feedbackId");
		String text = JsonPath.read(content, "$.rows[0].text");
		String title = JsonPath.read(content, "$.rows[0].title");
		FeedbackDO feedback = feedbackDao.getById(ID);
		assertEquals("aaa",title);
		assertEquals("bbb",text);
		assertEquals("aaa",feedback.getTitle());
		assertEquals("bbb",feedback.getText());
	}

	@Test
	public void testGetFeedbackByID() throws Exception {
		FeedbackDO feedback = testUtil.createFeedback();
		String ID = feedback.getFeedbackId();
		ResultActions action = mockMvc.perform(MockMvcRequestBuilders.get("/v1/feedback/"+ID+".json"));
		action.andExpect(status().isOk());
		String content = action.andReturn().getResponse().getContentAsString();
		int total = JsonPath.read(content, "$.total");
		String text = JsonPath.read(content, "$.rows[0].text");
		String title = JsonPath.read(content, "$.rows[0].title");
		String createDate = JsonPath.read(content,"$.rows[0].createDate");
		assertEquals(1,total);
		assertEquals(feedback.getText(),text);
		assertEquals(feedback.getTitle(),title);
		assertEquals(feedback.getCreateDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),createDate);
	}
	
	@Test
	public void testGetFeedbackByID3() throws Exception {
		FeedbackDO feedback = testUtil.createFeedback();
		String ID = feedback.getFeedbackId();
		ResultActions action = mockMvc.perform(MockMvcRequestBuilders.get("/v1/feedback/"+ID+".xml"));
		action.andExpect(status().isOk());
		action.andExpect(xpath("/response/total").number(1D));
		action.andExpect(xpath("/response/rows/array/text").string(feedback.getText()));
		action.andExpect(xpath("/response/rows/array/title").string(feedback.getTitle()));
	}

	@Test
	public void testGetFeedbackByParameter() throws Exception {
		FeedbackDO feedback = testUtil.createFeedback();
	    String param = "feedbackId='"+feedback.getFeedbackId()+"' and text='"+feedback.getText()+"'";
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/v1/feedback");
	    request.param(PublicParameter.FORMAT.value(), "json");
	    request.param(PublicParameter.SEARCH.value(), param);
		ResultActions action = mockMvc.perform(request);
		action.andExpect(status().isOk());
		String content = action.andReturn().getResponse().getContentAsString();
		int total = JsonPath.read(content, "$.total");
		String feedbackId = JsonPath.read(content, "$.rows[0].feedbackId");
		String text = JsonPath.read(content, "$.rows[0].text");
		assertEquals(1,total);
		assertEquals(feedback.getFeedbackId(),feedbackId);
		assertEquals(feedback.getText(),text);
	}

}
