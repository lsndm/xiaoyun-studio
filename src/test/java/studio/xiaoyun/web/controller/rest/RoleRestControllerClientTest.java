package studio.xiaoyun.web.controller.rest;

import com.alibaba.fastjson.JSON;
import com.jayway.jsonpath.JsonPath;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import studio.xiaoyun.core.constant.Permission;
import studio.xiaoyun.core.constant.SystemRole;
import studio.xiaoyun.core.dao.PermissionDao;
import studio.xiaoyun.core.dao.RoleDao;
import studio.xiaoyun.core.dao.TestUtil;
import studio.xiaoyun.core.pojo.PermissionDO;
import studio.xiaoyun.core.pojo.RoleDO;
import studio.xiaoyun.core.exception.InvalidParameterException;
import studio.xiaoyun.core.query.PermissionQuery;
import studio.xiaoyun.web.PublicParameter;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextHierarchy({
        @ContextConfiguration(name = "parent", locations = "classpath:config/spring-context-test.xml"),
        @ContextConfiguration(name = "child", locations = "classpath:config/spring-mvc-test.xml")
})
public class RoleRestControllerClientTest {
    @Resource
    private WebApplicationContext wac;
    private MockMvc mockMvc;
    @Resource
    private RoleDao roleDao;
    @Resource
    private PermissionDao permissionDao;
    @Resource
    private TestUtil testUtil;

    @Before
    public void setup() throws Exception {
        testUtil.loginForAdmin();
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void createRole() throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.put("/v1/role");
        request.param("name", "23fdfdfc");
        request.param("description", "2dd");
        request.param(PublicParameter.FORMAT.value(), "json");

        ResultActions action = mockMvc.perform(request);
        action.andExpect(status().is2xxSuccessful());
        String content = action.andReturn().getResponse().getContentAsString();
        String ID = JsonPath.read(content, "$.rows[0].roleId");
        String name = JsonPath.read(content, "$.rows[0].name");
        String description = JsonPath.read(content, "$.rows[0].description");
        RoleDO role = roleDao.getById(ID);

        assertEquals("23fdfdfc", name);
        assertEquals("2dd", description);
        assertEquals(name, role.getName());
        assertEquals(description, role.getDescription());
    }

    @Test
    public void updateRole() throws Exception {
        RoleDO role = testUtil.createRole();
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/v1/role/" + role.getRoleId());
        request.param("name", "23fddsffdfc");
        request.param("description", "ddd");
        request.param(PublicParameter.FORMAT.value(), "json");

        ResultActions action = mockMvc.perform(request);
        action.andExpect(status().is2xxSuccessful());
        RoleDO role2 = roleDao.getById(role.getRoleId());

        assertEquals("23fddsffdfc", role2.getName());
        assertEquals("ddd", role2.getDescription());
    }

    @Test(expected = InvalidParameterException.class)
    public void deleteRole() throws Exception {
        RoleDO role = testUtil.createRole();
        String roleId = JSON.toJSONString(Collections.singletonList(role.getRoleId()));
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.delete("/v1/role.json");
        request.param("roleId", roleId);
        ResultActions action = mockMvc.perform(request);
        action.andExpect(status().is2xxSuccessful());
        roleDao.getById(role.getRoleId());
    }

    @Test
    public void getRoleByParameter() throws Exception {
        RoleDO role = testUtil.createRole();
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/v1/role.xml");
        request.param(PublicParameter.SEARCH.value(), "roleId='" + role.getRoleId() + "'");
        ResultActions action = mockMvc.perform(request);
        action.andExpect(xpath("/response/total").number(1D));
    }

    @Test
    public void getPermissionByRoleId() throws Exception {
        String uri = "/v1/role/" + SystemRole.ADMIN.getRoleId() + "/permission.xml";
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get(uri);
        ResultActions action = mockMvc.perform(request);
        action.andExpect(xpath("/response/total").number(new Double(Permission.values().length)));
    }

    @Test
    public void setRolePermission() throws Exception {
        RoleDO role = testUtil.createRole();
        PermissionQuery param = new PermissionQuery();
        param.setName(Permission.ROLE_DELETE_ALL);
        List<PermissionDO> list = permissionDao.listPermissionByParameter(param);
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/v1/role/"+role.getRoleId()+"/permission.xml");
        request.param("permissionId","[\""+list.get(0).getPermissionId()+"\"]");
        ResultActions action = mockMvc.perform(request);
        action.andExpect(status().is2xxSuccessful());
        long count = permissionDao.countPermissionByRoleId(role.getRoleId(),null);
        assertEquals(1,count);
    }

}