package studio.xiaoyun.web.dto;

import java.util.Date;

import studio.xiaoyun.core.query.criterion.TestEnum1;
import studio.xiaoyun.core.query.criterion.TestEnum2;
import studio.xiaoyun.core.query.criterion.TestEnum3;

public class TestResource implements AbstractDTO {
	private Integer ID1;
	private Long ID2;
	private String name;
	private Date date;
	private TestEnum1 enum1;
	private TestEnum2 enum2;
	private TestEnum3 enum3;

	public Date getDate() {
		return date;
	}

	public Integer getID1() {
		return ID1;
	}

	public Long getID2() {
		return ID2;
	}

	public String getName() {
		return name;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setID1(Integer iD1) {
		ID1 = iD1;
	}

	public void setID2(Long iD2) {
		ID2 = iD2;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TestEnum3 getEnum3() {
		return enum3;
	}

	public void setEnum3(TestEnum3 enum3) {
		this.enum3 = enum3;
	}

	public TestEnum2 getEnum2() {
		return enum2;
	}

	public void setEnum2(TestEnum2 enum2) {
		this.enum2 = enum2;
	}

	public TestEnum1 getEnum1() {
		return enum1;
	}

	public void setEnum1(TestEnum1 enum1) {
		this.enum1 = enum1;
	}

}
