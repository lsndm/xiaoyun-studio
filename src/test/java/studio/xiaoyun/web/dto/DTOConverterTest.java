package studio.xiaoyun.web.dto;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import studio.xiaoyun.core.dao.FeedbackDao;
import studio.xiaoyun.core.dao.TestUtil;
import studio.xiaoyun.core.pojo.FeedbackDO;
import studio.xiaoyun.core.query.FeedbackQuery;
import studio.xiaoyun.core.query.AbstractQuery;
import studio.xiaoyun.core.query.TestQuery;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:config/spring-context-test.xml")
public class DTOConverterTest {
	@Resource
	private DTOConverter util;
	@Resource
	private FeedbackDao dao;
	@Resource
	private TestUtil testUtil;

	@Test
	public void toResource() {
		FeedbackDO feedback = testUtil.createFeedback();
		List<FeedbackDO> list = Collections.singletonList(feedback);
		List<FeedbackDTO> resources = util.toDto(list,null,FeedbackDTO.class);
		FeedbackDTO resource = resources.get(0);
		assertEquals(feedback.getText(),resource.getText());
		assertEquals(feedback.getTitle(),resource.getTitle());
		assertEquals(feedback.getFeedbackId(),resource.getFeedbackId());
	}

	@Test
	public void testSetNull() {
		FeedbackDO feedback = testUtil.createFeedback();
		List<FeedbackDO> list = Collections.singletonList(feedback);
		FeedbackQuery param = new FeedbackQuery();
		param.addIncludeField("text","createDate");
		List<FeedbackDTO> resources = util.toDto(list,param,FeedbackDTO.class);
		FeedbackDTO resource = resources.get(0);
		assertEquals(feedback.getText(),resource.getText());
		assertNull(resource.getTitle());
		assertNotNull(resource.getCreateDate());
	}

	@Test
	public void testSetNull2() {
		TestResource test = new TestResource();
		test.setDate(new Date());
		test.setName("aa");
		test.setID1(1);
		AbstractQuery param = new TestQuery();
		param.addIncludeField("date","name");
		DTOConverter util = new DTOConverter();
		util.setNull(Collections.singletonList(test), param);
		assertNotNull(test.getDate());
		assertNull(test.getID1());
		assertEquals("aa",test.getName());
	}
}
