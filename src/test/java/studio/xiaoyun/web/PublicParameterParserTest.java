package studio.xiaoyun.web;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import studio.xiaoyun.core.exception.XyException;
import studio.xiaoyun.core.query.RoleQuery;

import java.util.List;

import static org.junit.Assert.*;

public class PublicParameterParserTest {
    @Test
    public void testGetIncludeFields() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(PublicParameter.FIELD.value(), "name,ID1");
        List<String> list = PublicParameterParser.getIncludeFields(request);
        assertEquals(2,list.size());
        assertEquals("name",list.get(0));
        assertEquals("ID1",list.get(1));
    }

    @Test
    public void testGetSortFields() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(PublicParameter.SORT.value(), "name asc,ID1 desc");
        List<Object[]> list = PublicParameterParser.getSortFields(request);
        assertEquals(2,list.size());
        assertEquals("name",list.get(0)[0]);
        assertTrue((Boolean)list.get(0)[1]);
        assertEquals("ID1",list.get(1)[0]);
        assertFalse((Boolean)list.get(1)[1]);
    }

    @Test
    public void testGetStart() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(PublicParameter.START.value(), "1");
        int start = PublicParameterParser.getStart(request);
        assertEquals(1,start);
    }

    @Test
    public void testGetStart2() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(PublicParameter.PAGE.value(), "2");
        request.addParameter(PublicParameter.ROWS.value(), "2");
        int start = PublicParameterParser.getStart(request);
        assertEquals(2,start);
    }

    @Test
    public void testGetRows() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(PublicParameter.ROWS.value(), "2");
        int rows = PublicParameterParser.getRows(request);
        assertEquals(2,rows);
    }

    @Test
    public void testIn(){
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(PublicParameter.SEARCH.value(), "roleId in ('a','b')");
        RoleQuery param = PublicParameterParser.getParameter(request, RoleQuery.class);
        String query = param.getQuery();
        List<Object> list = param.getQueryValue();
        assertEquals("roleId in (?,?)",query);
        assertEquals(2,list.size());
        assertEquals("a",list.get(0).toString());
        assertEquals("b",list.get(1).toString());
    }

    @Test
    public void testEquals(){
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(PublicParameter.SEARCH.value(), "roleId =a");
        RoleQuery param = PublicParameterParser.getParameter(request, RoleQuery.class);
        String query = param.getQuery();
        List<Object> list = param.getQueryValue();
        assertEquals("roleId = ?",query);
        assertEquals(1,list.size());
        assertEquals("a",list.get(0).toString());
    }

    @Test
    public void testGe(){
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(PublicParameter.SEARCH.value(), "roleId >=a ");
        RoleQuery param = PublicParameterParser.getParameter(request, RoleQuery.class);
        String query = param.getQuery();
        List<Object> list = param.getQueryValue();
        assertEquals("roleId >= ?",query);
        assertEquals(1,list.size());
        assertEquals("a",list.get(0).toString());
    }

    @Test
    public void testGt(){
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(PublicParameter.SEARCH.value(), "roleId > 'a' ");
        RoleQuery param = PublicParameterParser.getParameter(request, RoleQuery.class);
        String query = param.getQuery();
        List<Object> list = param.getQueryValue();
        assertEquals("roleId > ?",query);
        assertEquals(1,list.size());
        assertEquals("a",list.get(0).toString());
    }

    @Test
    public void testLe(){
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(PublicParameter.SEARCH.value(), "roleId <=a ");
        RoleQuery param = PublicParameterParser.getParameter(request, RoleQuery.class);
        String query = param.getQuery();
        List<Object> list = param.getQueryValue();
        assertEquals("roleId <= ?",query);
        assertEquals(1,list.size());
        assertEquals("a",list.get(0).toString());
    }

    @Test
    public void testLt(){
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(PublicParameter.SEARCH.value(), "roleId < 'a' ");
        RoleQuery param = PublicParameterParser.getParameter(request, RoleQuery.class);
        String query = param.getQuery();
        List<Object> list = param.getQueryValue();
        assertEquals("roleId < ?",query);
        assertEquals(1,list.size());
        assertEquals("a",list.get(0).toString());
    }

    @Test
    public void testLike(){
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(PublicParameter.SEARCH.value(), "roleId like 'a' ");
        RoleQuery param = PublicParameterParser.getParameter(request, RoleQuery.class);
        String query = param.getQuery();
        List<Object> list = param.getQueryValue();
        assertEquals("roleId like ?",query);
        assertEquals(1,list.size());
        assertEquals("a",list.get(0).toString());
    }

    @Test
    public void testLike2(){
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(PublicParameter.SEARCH.value(), "roleId like '?a*' ");
        RoleQuery param = PublicParameterParser.getParameter(request, RoleQuery.class);
        String query = param.getQuery();
        List<Object> list = param.getQueryValue();
        assertEquals("roleId like ?",query);
        assertEquals(1,list.size());
        assertEquals("_a%",list.get(0).toString());
    }

    @Test
    public void testNotEquals(){
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(PublicParameter.SEARCH.value(), "roleId != 'a' ");
        RoleQuery param = PublicParameterParser.getParameter(request, RoleQuery.class);
        String query = param.getQuery();
        List<Object> list = param.getQueryValue();
        assertEquals("roleId <> ?",query);
        assertEquals(1,list.size());
        assertEquals("a",list.get(0).toString());
    }

    @Test
    public void testAnd(){
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(PublicParameter.SEARCH.value(), "roleId=a and name >'b' and name < 'c'");
        RoleQuery param = PublicParameterParser.getParameter(request, RoleQuery.class);
        String query = param.getQuery();
        List<Object> list = param.getQueryValue();
        assertEquals("roleId = ? and name > ? and name < ?",query);
        assertEquals(3,list.size());
        assertEquals("a",list.get(0).toString());
        assertEquals("b",list.get(1).toString());
        assertEquals("c",list.get(2).toString());
    }

    @Test
    public void testOr(){
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(PublicParameter.SEARCH.value(), "roleId=a or name >'b' or name < 'c' ");
        RoleQuery param = PublicParameterParser.getParameter(request, RoleQuery.class);
        String query = param.getQuery();
        List<Object> list = param.getQueryValue();
        assertEquals("roleId = ? or name > ? or name < ?",query);
        assertEquals(3,list.size());
        assertEquals("a",list.get(0).toString());
        assertEquals("b",list.get(1).toString());
        assertEquals("c",list.get(2).toString());
    }

    @Test
    public void testAndOr(){
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(PublicParameter.SEARCH.value(), "roleId=a and (name >'b' or name < 'c') ");
        RoleQuery param = PublicParameterParser.getParameter(request, RoleQuery.class);
        String query = param.getQuery();
        List<Object> list = param.getQueryValue();
        assertEquals("roleId = ? and (name > ? or name < ?)",query);
        assertEquals(3,list.size());
        assertEquals("a",list.get(0).toString());
        assertEquals("b",list.get(1).toString());
        assertEquals("c",list.get(2).toString());
    }

    @Test
    public void testOrAnd(){
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(PublicParameter.SEARCH.value(), "roleId=a or (name >'b' and name < 'c') ");
        RoleQuery param = PublicParameterParser.getParameter(request, RoleQuery.class);
        String query = param.getQuery();
        List<Object> list = param.getQueryValue();
        assertEquals("roleId = ? or (name > ? and name < ?)",query);
        assertEquals(3,list.size());
        assertEquals("a",list.get(0).toString());
        assertEquals("b",list.get(1).toString());
        assertEquals("c",list.get(2).toString());
    }

    @Test(expected = XyException.class)
    public void testNull(){
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(PublicParameter.SEARCH.value(), "roleId =  and name=b");
        PublicParameterParser.getParameter(request, RoleQuery.class);
    }

    @Test(expected = XyException.class)
    public void test(){
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(PublicParameter.SEARCH.value(), "roleId = 'a' and 1=1");
        PublicParameterParser.getParameter(request, RoleQuery.class);
    }

}