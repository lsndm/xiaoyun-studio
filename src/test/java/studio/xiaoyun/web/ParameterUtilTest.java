package studio.xiaoyun.web;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import studio.xiaoyun.core.exception.XyException;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class ParameterUtilTest {
	@Test
	public void getBoolean() throws Exception {
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.addParameter("a", "true");
		Boolean a = ParameterUtil.getBoolean(request,"a",null);
		assertTrue(a);
	}

	@Test
	public void getBoolean1() throws Exception {
		MockHttpServletRequest request = new MockHttpServletRequest();
		Boolean a = ParameterUtil.getBoolean(request,"a",null);
		assertNull(a);
	}

	@Test
	public void getBoolean2() throws Exception {
		MockHttpServletRequest request = new MockHttpServletRequest();
		Boolean a = ParameterUtil.getBoolean(request,"a",true);
		assertTrue(a);
	}

	@Test
	public void getBoolean3() throws Exception {
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.addParameter("a", "aaa");
		Boolean a = ParameterUtil.getBoolean(request,"a",null);
		assertFalse(a);
	}

	@Test
	public void testGetIntHttpServletRequestString() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.addParameter("a", "1");
		int i = ParameterUtil.getInt(request, "a",null);
		assertEquals(1,i);
	}
	
	@Test
	public void testGetIntHttpServletRequestString2() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		Integer i = ParameterUtil.getInt(request, "a",null);
		assertNull(i);
	}
	
	@Test(expected=XyException.class)
	public void testGetIntHttpServletRequestString3() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.addParameter("a", "a");
		ParameterUtil.getInt(request, "a",null);
	}

	@Test
	public void testGetIntHttpServletRequestStringInteger() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.addParameter("a", "1");
		int i = ParameterUtil.getInt(request, "a",0);
		assertEquals(1,i);
	}
	
	@Test
	public void testGetIntHttpServletRequestStringInteger2() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		int i = ParameterUtil.getInt(request, "a",0);
		assertEquals(0,i);
	}

	@Test
	public void testGetLongHttpServletRequestString() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.addParameter("a", "1");
		long i = ParameterUtil.getLong(request, "a",null);
		assertEquals(1L,i);
	}
	
	@Test
	public void testGetLongHttpServletRequestString2() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		Long i = ParameterUtil.getLong(request, "a",null);
		assertNull(i);
	}

	@Test(expected=XyException.class)
	public void testGetLongHttpServletRequestString3() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.addParameter("a", "1a");
		ParameterUtil.getLong(request, "a",null);
	}
	
	@Test
	public void testGetLongHttpServletRequestStringLong() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		long i = ParameterUtil.getLong(request, "a",1L);
		assertEquals(1L,i);
	}

	@Test
	public void testGetDoubleHttpServletRequestString() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.addParameter("a", "1");
		Double i = ParameterUtil.getDouble(request, "a",null);
		assertEquals(new Double(1),i);
	}
	
	@Test(expected=XyException.class)
	public void testGetDoubleHttpServletRequestString2() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.addParameter("a", "1a");
		ParameterUtil.getDouble(request, "a",null);
	}
	
	@Test
	public void testGetDoubleHttpServletRequestString3() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		Double i = ParameterUtil.getDouble(request, "a",null);
		assertNull(i);
	}

	@Test
	public void testGetDoubleHttpServletRequestStringDouble() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		Double i = ParameterUtil.getDouble(request, "a",1D);
		assertEquals(new Double(1),i);
	}

	@Test
	public void testGetString() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.addParameter("a", "你好");
		String s = ParameterUtil.getString(request, "a","s");
		assertEquals("你好",s);
	}
	
	@Test
	public void testGetString2() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		String s = ParameterUtil.getString(request, "a","s");
		assertEquals("s",s);
	}

	@Test
	public void testGetDateHttpServletRequestString() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.addParameter("a", "2010-01-01");
		Date d = ParameterUtil.getDate(request, "a");
		assertEquals("2010-01-01",new SimpleDateFormat("yyyy-MM-dd").format(d));
	}
	
	@Test
	public void testGetDateHttpServletRequestString2() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		Date d = ParameterUtil.getDate(request, "a");
		assertNull(d);
	}
	
	@Test(expected=XyException.class)
	public void testGetDateHttpServletRequestString3() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.addParameter("a", "2010-0sssss1-01");
		ParameterUtil.getDate(request, "a");
	}

	@Test
	public void testGetDateHttpServletRequestStringString() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.addParameter("a", "2010/01/01");
		Date d = ParameterUtil.getDate(request, "a","yyyy/MM/dd");
		assertEquals("2010-01-01",new SimpleDateFormat("yyyy-MM-dd").format(d));
	}

}
