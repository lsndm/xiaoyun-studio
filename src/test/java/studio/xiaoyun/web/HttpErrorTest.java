package studio.xiaoyun.web;

import org.junit.Test;
import studio.xiaoyun.core.exception.XyException;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class HttpErrorTest {
    @Test
    public void setStack() throws Exception {
        XyException x = getException();
        HttpError error = new HttpError();
        error.setStack(x);
        List<String> list = error.getStack();
        assertTrue(list.size()>2);
        assertTrue(list.get(0).contains(x.getClass().getName()));
        assertTrue(list.get(0).contains(x.getMessage()));
    }

    private XyException getException()
    {
        XyException xyException = new XyException("ss");
        XyException xyException2 = new XyException("ss");
        xyException.addSuppressed(xyException2);
        return xyException;
    }

}