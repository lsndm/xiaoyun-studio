package studio.xiaoyun.core;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;


public class SystemInitTest {
    @Test
    public void loadConfigXML() throws Exception {
        SystemInit init = new SystemInit();
        init.initPath();
        init.loadConfigXML();
        assertNotNull(Config.getBasePath());
    }

}