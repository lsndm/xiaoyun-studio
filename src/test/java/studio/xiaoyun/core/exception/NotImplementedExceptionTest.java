package studio.xiaoyun.core.exception;

import org.junit.Test;

import static org.junit.Assert.*;


public class NotImplementedExceptionTest {

    @Test
    public void testGetMessage(){
        NotImplementedException e = new NotImplementedException();
        String message = e.getMessage();
        assertEquals("方法未实现:"+NotImplementedExceptionTest.class.getName()+"#testGetMessage",message);
    }

    @Test
    public void testToString(){
        NotImplementedException e = new NotImplementedException();
        String message = e.toString();
        assertEquals("方法未实现:"+NotImplementedExceptionTest.class.getName()+"#testToString",message);
    }

}