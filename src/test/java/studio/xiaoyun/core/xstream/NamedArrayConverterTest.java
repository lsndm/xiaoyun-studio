package studio.xiaoyun.core.xstream;

import com.thoughtworks.xstream.XStream;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class NamedArrayConverterTest {
    private XPath xpath = XPathFactory.newInstance().newXPath();

    @Test
    public void writeItem() throws Exception {
        XStream xstream = new XStream();
        xstream.registerConverter(new NamedArrayConverter(xstream.getMapper(),"array"));
        xstream.alias("test",TestBean.class);
        TestBean test = new TestBean();
        test.setStrings(new String[]{"a","b"});
        String xml = xstream.toXML(test);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new InputSource(new StringReader(xml)));
        String value = (String)xpath.evaluate("/test/strings/array[1]", doc, XPathConstants.STRING);
        String value2 = (String)xpath.evaluate("/test/strings/array[2]", doc, XPathConstants.STRING);
        assertEquals("a",value);
        assertEquals("b",value2);
    }

    @Test
    public void readItem() throws Exception {
        String xml = "<test><strings><array class=\"string\">a</array><array class=\"string\">b</array></strings></test>";
        XStream xstream = new XStream();
        xstream.registerConverter(new NamedArrayConverter(xstream.getMapper(),"array"));
        xstream.alias("test",TestBean.class);
        TestBean test = (TestBean)xstream.fromXML(xml);
        String[] str = test.getStrings();
        assertNotNull(str);
        assertEquals(2,str.length);
        assertEquals("a",str[0]);
        assertEquals("b",str[1]);
    }

}