package studio.xiaoyun.core.xstream;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.extended.NamedCollectionConverter;
import com.thoughtworks.xstream.converters.extended.NamedMapConverter;
import com.thoughtworks.xstream.io.xml.CompactWriter;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import studio.xiaoyun.web.ErrorCode;
import studio.xiaoyun.core.constant.SystemUser;
import studio.xiaoyun.web.HttpError;
import studio.xiaoyun.web.HttpResponse;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class XStreamTest {
    private XPath xpath = XPathFactory.newInstance().newXPath();

    private Document getDocumennt(Object obj) throws Exception {
        XStream xstream = new XStream();
        xstream.autodetectAnnotations(true);
        xstream.registerConverter(new NamedCollectionConverter(xstream.getMapper(), "array", null));
        xstream.registerConverter(new NamedSingletonCollectionConverter(xstream.getMapper(), "array"));
        xstream.registerConverter(new NamedArrayConverter(xstream.getMapper(), "array"));
        xstream.registerConverter(new CustomDateConverter("yyyy-MM-dd HH:mm:ss"));
        xstream.registerConverter(new LocalDateConverter("yyyy-MM-dd"));
        xstream.registerConverter(new LocalTimeConverter("HH:mm:ss"));
        xstream.registerConverter(new LocalDateTimeConverter("yyyy-MM-dd HH:mm:ss"));
        xstream.registerConverter(new NamedMapConverter(xstream.getMapper(), "array", "key", String.class, "value", String.class));
        xstream.alias("test",TestBean.class);

        OutputStream output = new ByteArrayOutputStream();
        xstream.marshal(obj, new CompactWriter(new OutputStreamWriter(output)));
        String xml = output.toString();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new InputSource(new StringReader(xml)));
        return doc;
    }

    @Test
    public void httpResponseToXml() throws Exception {
        HttpResponse response = new HttpResponse();
        response.setTotal(10L);
        HttpError error = new HttpError();
        error.setMessage("aa");
        error.setCode(ErrorCode.DISABLED_ACCOUNT);
        response.setError(error);
        TestBean resource = new TestBean();
        resource.setLocalDateTime(LocalDateTime.now());
        List<TestBean> list = new ArrayList<>();
        list.add(resource);
        response.setRows(list);

        Document doc = getDocumennt(response);
        String total = (String) xpath.evaluate("/response/total", doc, XPathConstants.STRING);
        String time = (String)xpath.evaluate("/response/rows/array[1]/localDateTime", doc, XPathConstants.STRING);
        String errorCode = (String)xpath.evaluate("/response/error/code", doc, XPathConstants.STRING);
        String errorMsg = (String)xpath.evaluate("/response/error/message", doc, XPathConstants.STRING);
        assertEquals("10",total);
        assertEquals(resource.getLocalDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),time);
        assertEquals(error.getMessage(),errorMsg);
        assertEquals(error.getCode().name(),errorCode);
    }

    @Test
    public void intToXml()throws Exception {
        TestBean test = new TestBean();
        test.setInteger(11);
        Document doc = getDocumennt(test);
        String value = (String)xpath.evaluate("/test/integer", doc, XPathConstants.STRING);
        assertEquals(test.getInteger(),Integer.valueOf(value));
    }

    @Test
    public void doubleToXml()throws Exception {
        TestBean test = new TestBean();
        test.setaDouble(1.1);
        Document doc = getDocumennt(test);
        String value = (String)xpath.evaluate("/test/aDouble", doc, XPathConstants.STRING);
        assertEquals(test.getaDouble(),Double.valueOf(value));
    }

    @Test
    public void strToXml() throws Exception {
        TestBean test = new TestBean();
        test.setStr("a");
        Document doc = getDocumennt(test);
        String value = (String)xpath.evaluate("/test/str", doc, XPathConstants.STRING);
        assertEquals(test.getStr(),value);
    }

    @Test
    public void enumToXml() throws Exception {
        TestBean test = new TestBean();
        test.setSystemUser(SystemUser.USER);
        Document doc = getDocumennt(test);
        String value = (String)xpath.evaluate("/test/systemUser", doc, XPathConstants.STRING);
        assertEquals(test.getSystemUser().name(),value);
    }

    @Test
    public void xmlToEnum()throws Exception{
        String xml = "<test><systemUser>USER</systemUser></test>";
        XStream xstream = new XStream();
        xstream.alias("test",TestBean.class);
        TestBean test = (TestBean)xstream.fromXML(xml);
        assertEquals("USER",test.getSystemUser().name());
    }

}
