package studio.xiaoyun.core.xstream;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.CompactWriter;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;

public class LocalDateConverterTest {
    private XPath xpath = XPathFactory.newInstance().newXPath();

    @Test
    public void fromString() throws Exception {
        String xml = "<test><localDate>2016-12-31</localDate></test>";
        XStream xstream = new XStream();
        xstream.registerConverter(new LocalDateConverter("yyyy-MM-dd"));
        xstream.alias("test",TestBean.class);
        TestBean test = (TestBean)xstream.fromXML(xml);
        assertEquals("2016-12-31",test.getLocalDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
    }

    @Test
    public void toString2() throws Exception {
        XStream xstream = new XStream();
        xstream.registerConverter(new LocalDateConverter("yyyy-MM-dd"));
        xstream.alias("test",TestBean.class);
        TestBean test = new TestBean();
        test.setLocalDate(LocalDate.now());
        OutputStream output = new ByteArrayOutputStream();
        xstream.marshal(test, new CompactWriter(new OutputStreamWriter(output)));
        String xml = output.toString();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new InputSource(new StringReader(xml)));
        String value = (String)xpath.evaluate("/test/localDate", doc, XPathConstants.STRING);
        assertEquals(test.getLocalDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")),value);
    }

}