package studio.xiaoyun.core.xstream;

import com.thoughtworks.xstream.XStream;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.*;

public class LocalDateTimeConverterTest {
    private XPath xpath = XPathFactory.newInstance().newXPath();

    @Test
    public void fromString() throws Exception {
        String xml = "<test><localDateTime>2016-12-31 10:11:12</localDateTime></test>";
        XStream xstream = new XStream();
        xstream.registerConverter(new LocalDateTimeConverter("yyyy-MM-dd HH:mm:ss"));
        xstream.alias("test",TestBean.class);
        TestBean test = (TestBean)xstream.fromXML(xml);
        assertEquals("2016-12-31 10:11:12",test.getLocalDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
    }

    @Test
    public void toString2() throws Exception {
        XStream xstream = new XStream();
        xstream.registerConverter(new LocalDateTimeConverter("yyyy-MM-dd HH:mm:ss"));
        xstream.alias("test",TestBean.class);
        TestBean test = new TestBean();
        test.setLocalDateTime(LocalDateTime.now());
        String xml = xstream.toXML(test);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new InputSource(new StringReader(xml)));
        String value = (String)xpath.evaluate("/test/localDateTime", doc, XPathConstants.STRING);
        assertEquals(test.getLocalDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),value);
    }

}