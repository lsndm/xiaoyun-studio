package studio.xiaoyun.core.xstream;

import com.thoughtworks.xstream.XStream;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class NamedSingletonCollectionConverterTest {
    private XPath xpath = XPathFactory.newInstance().newXPath();

    @Test
    public void writeItem() throws Exception {
        XStream xstream = new XStream();
        xstream.registerConverter(new NamedSingletonCollectionConverter(xstream.getMapper(),"array"));
        xstream.alias("test",TestBean.class);
        TestBean test = new TestBean();
        TestBean test2 = new TestBean();
        test2.setStr("aa");
        test.setList(Collections.singletonList(test2));
        String xml = xstream.toXML(test);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new InputSource(new StringReader(xml)));
        String value = (String)xpath.evaluate("/test/list/array[1]/str", doc, XPathConstants.STRING);
        assertEquals("aa",value);
    }

    @Test
    public void readItem() throws Exception {
        String xml = "<test>\n" +
                "  <list class=\"singleton-list\">\n" +
                "    <array class=\"test\">\n" +
                "      <str>aa</str>\n" +
                "    </array>\n" +
                "  </list>\n" +
                "</test>";
        XStream xstream = new XStream();
        xstream.registerConverter(new NamedSingletonCollectionConverter(xstream.getMapper(),"array"));
        xstream.alias("test",TestBean.class);
        TestBean test = (TestBean)xstream.fromXML(xml);
        assertEquals(1,test.getList().size());
        assertEquals("aa",test.getList().get(0).getStr());
    }

}