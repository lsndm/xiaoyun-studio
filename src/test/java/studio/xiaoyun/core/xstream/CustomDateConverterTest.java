package studio.xiaoyun.core.xstream;

import com.thoughtworks.xstream.XStream;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class CustomDateConverterTest {
    private XPath xpath = XPathFactory.newInstance().newXPath();

    @Test
    public void dateToXml() throws Exception {
        XStream xstream = new XStream();
        xstream.registerConverter(new CustomDateConverter("yyyy-MM-dd HH:mm:ss"));
        xstream.alias("test",TestBean.class);
        TestBean test = new TestBean();
        test.setDate(new Date());
        String xml = xstream.toXML(test);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new InputSource(new StringReader(xml)));
        String value = (String)xpath.evaluate("/test/date", doc, XPathConstants.STRING);
        assertEquals(DateFormatUtils.format(test.getDate(),"yyyy-MM-dd HH:mm:ss"),value);
    }

    @Test
    public void xmlToDate(){
        String xml = "<test><date>2016-12-31 12:56:16</date></test>";
        XStream xstream = new XStream();
        xstream.registerConverter(new CustomDateConverter("yyyy-MM-dd HH:mm:ss"));
        xstream.alias("test",TestBean.class);
        TestBean test = (TestBean)xstream.fromXML(xml);
        assertEquals("2016-12-31 12:56:16",DateFormatUtils.format(test.getDate(),"yyyy-MM-dd HH:mm:ss"));
    }

}