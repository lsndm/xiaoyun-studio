package studio.xiaoyun.core.xstream;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.extended.NamedCollectionConverter;
import com.thoughtworks.xstream.io.xml.CompactWriter;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import studio.xiaoyun.web.HttpResponse;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class StringKeyMapConverterTest {
    private XPath xpath = XPathFactory.newInstance().newXPath();

    private Document getDocumennt(Object obj) throws Exception {
        XStream xstream = new XStream();
        xstream.alias("test",TestBean.class);
        xstream.autodetectAnnotations(true);
        xstream.registerConverter(new StringKeyMapConverter(xstream.getMapper()));
        xstream.registerConverter(new NamedCollectionConverter(xstream.getMapper(), "array", null));
        xstream.registerConverter(new NamedSingletonCollectionConverter(xstream.getMapper(), "array"));

        OutputStream output = new ByteArrayOutputStream();
        xstream.marshal(obj, new CompactWriter(new OutputStreamWriter(output)));
        String xml = output.toString();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new InputSource(new StringReader(xml)));
        return doc;
    }

    @Test
    public void stringKeyMapToXml()throws Exception {
        TestBean test = new TestBean();
        Map<String,Object> map= new HashMap<>();
        map.put("name","user");
        map.put("code",11);
        test.setMap(map);
        Document doc = getDocumennt(test);
        String name = (String)xpath.evaluate("/test/map/name", doc, XPathConstants.STRING);
        String code = (String)xpath.evaluate("/test/map/code", doc, XPathConstants.STRING);
        assertEquals("user",name);
        assertEquals("11",code);
    }

    @Test
    public void stringKeyMapToXml2()throws Exception {
        TestBean test = new TestBean();
        TestBean test2 = new TestBean();
        test2.setStr("aa");
        Map<String,Object> map= new HashMap<>();
        map.put("name","user");
        map.put("bean",test2);
        test.setMap(map);
        Document doc = getDocumennt(test);
        String name = (String)xpath.evaluate("/test/map/name", doc, XPathConstants.STRING);
        String str = (String)xpath.evaluate("/test/map/bean/str", doc, XPathConstants.STRING);
        assertEquals("user",name);
        assertEquals("aa",str);
    }

    @Test
    public void listMapToXml() throws Exception {
        HttpResponse response = new HttpResponse();
        Map<String,String> map= new HashMap<>();
        map.put("name","user");
        map.put("code","11");
        response.setRows(Collections.singletonList(map));
        Document doc = getDocumennt(response);
        String name = (String)xpath.evaluate("/response/rows/array/name", doc, XPathConstants.STRING);
        String str = (String)xpath.evaluate("/response/rows/array/code", doc, XPathConstants.STRING);
        assertEquals("user",name);
        assertEquals("11",str);
    }

    @Test
    public void mapToXml() throws Exception {
        TestBean test = new TestBean();
        TestBean test2 = new TestBean();
        test2.setStr("aa");
        Map<Object,Object> map= new HashMap<>();
        map.put(1,"user");
        map.put("bean",test2);
        test.setMap(map);
        Document doc = getDocumennt(test);
        String v1 = (String)xpath.evaluate("/test/map/array[1]/key", doc, XPathConstants.STRING);
        String v2 = (String)xpath.evaluate("/test/map/array[1]/value", doc, XPathConstants.STRING);
        String v3 = (String)xpath.evaluate("/test/map/array[2]/key", doc, XPathConstants.STRING);
        String v4 = (String)xpath.evaluate("/test/map/array[2]/value/str", doc, XPathConstants.STRING);
        assertEquals("1",v1);
        assertEquals("user",v2);
        assertEquals("bean",v3);
        assertEquals("aa",v4);
    }

    @Test
    public void xmlToMap() throws Exception{
        XStream xstream = new XStream();
        xstream.alias("test",TestBean.class);
        xstream.registerConverter(new StringKeyMapConverter(xstream.getMapper()));
        String xml = "<test><map><code class=\"int\">11</code><name class=\"string\">user</name></map></test>";
        TestBean test = (TestBean)xstream.fromXML(xml);
        Map map = test.getMap();
        assertEquals(11,map.get("code"));
        assertEquals("user",map.get("name"));
    }

    @Test
    public void xmlToMap2() throws Exception{
        XStream xstream = new XStream();
        xstream.alias("test",TestBean.class);
        xstream.registerConverter(new StringKeyMapConverter(xstream.getMapper()));
        String xml = "<test><map><name class=\"string\">user</name><bean class=\"test\"><str>aa</str></bean></map></test>";
        TestBean test = (TestBean)xstream.fromXML(xml);
        Map map = test.getMap();
        TestBean bean = (TestBean)map.get("bean");
        assertEquals("user",map.get("name"));
        assertEquals("aa",bean.getStr());
    }

    @Test
    public void xmlToMap3() throws Exception{
        XStream xstream = new XStream();
        xstream.alias("test",TestBean.class);
        xstream.registerConverter(new StringKeyMapConverter(xstream.getMapper()));
        String xml = "<test><map><array class=\"array\"><key class=\"int\">1</key><value class=\"string\">user</value>" +
                "</array><array class=\"array\"><key class=\"string\">bean</key><value class=\"test\"><str>aa</str>" +
                "</value></array></map></test>";
        TestBean test = (TestBean)xstream.fromXML(xml);
        Map map = test.getMap();
        TestBean bean = (TestBean)map.get("bean");
        assertEquals("user",map.get(1));
        assertEquals("aa",bean.getStr());
    }


}