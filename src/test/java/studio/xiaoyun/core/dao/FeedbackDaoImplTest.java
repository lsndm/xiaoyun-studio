package studio.xiaoyun.core.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import studio.xiaoyun.core.pojo.FeedbackDO;
import studio.xiaoyun.core.query.FeedbackQuery;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:config/spring-context-test.xml")
public class FeedbackDaoImplTest {
    @Resource
    private FeedbackDaoImpl dao;
    @Resource
    private TestUtil testUtil;

    @Test
    public void getFeedbackCountByParameter() {
        FeedbackDO feedback = testUtil.createFeedback();
        FeedbackQuery param = new FeedbackQuery();
        param.setFeedbackId(feedback.getFeedbackId());
        long count = dao.countFeedbackByParameter(param);
        assertEquals(1,count);

    }

    @Test
    public void getFeedbacksByParameter() {
        FeedbackDO feedback = testUtil.createFeedback();
        FeedbackQuery param = new FeedbackQuery();
        param.setFeedbackId(feedback.getFeedbackId());
        List<FeedbackDO> list = dao.listFeedbackByParameter(param);
        assertEquals(1,list.size());
        assertEquals(feedback.getFeedbackId(),list.get(0).getFeedbackId());
    }

    @Test
    public void deleteFeedbacks() {
        FeedbackDO feedback = testUtil.createFeedback();
        FeedbackDO feedback2 = testUtil.createFeedback();
        dao.deleteFeedback(Arrays.asList(feedback.getFeedbackId(),feedback2.getFeedbackId()));
        String sql = "select count(*) from xy_feedback";
        long count = dao.countBySQL(sql);
        assertEquals(0,count);
    }

}