package studio.xiaoyun.core.dao;

import org.hibernate.query.NativeQuery;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import studio.xiaoyun.core.exception.InvalidParameterException;
import studio.xiaoyun.core.pojo.RoleDO;
import studio.xiaoyun.core.pojo.UserDO;
import studio.xiaoyun.core.query.RoleQuery;
import studio.xiaoyun.core.query.criterion.Query;

import javax.annotation.Resource;
import java.util.*;

import static org.junit.Assert.*;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:config/spring-context-test.xml")
public class RoleDaoImplTest {
    @Resource
    private TestUtil testUtil;
    @Resource
    private RoleDaoImpl dao;
    @Resource
    private UserDaoImpl userDao;

    @Test
    public void getById() throws Exception {
        RoleDO role = testUtil.createRole();
        RoleDO role2 = dao.getById(role.getRoleId());
        assertEquals("查询出的结果不一样",role.getName(),role2.getName());
    }

    @Test(expected = InvalidParameterException.class)
    public void getById2() throws Exception {
        dao.getById("99999999999999999");
    }

    @Test(expected = Exception.class)
    public void loadById2() throws Exception {
        RoleDO role = dao.loadById("9999999999999999");
        System.out.println(role.getRoleId());
    }

    @Test
    public void getRoleCountByParameter() throws Exception {
        RoleDO role = testUtil.createRole();
        RoleQuery param = new RoleQuery();
        param.setRoleId(role.getRoleId());
        long count = dao.countRoleByParameter(param);
        assertEquals("根据id查询角色数量时有问题",1,count);
    }

    @Test
    public void getRoleCountByParameter2() throws Exception {
        RoleDO role = testUtil.createRole();
        RoleQuery param = new RoleQuery();
        param.addQuery(Query.equals("name",role.getName()));
        long count = dao.countRoleByParameter(param);
        assertEquals("根据名称查询角色数量时有问题",1,count);
    }

    @Test
    public void getRoleCountByParameter3() throws Exception {
        String sql = "select count(*) from xy_role";
        long count1 = dao.countBySQL(sql);
        long count2 = dao.countRoleByParameter(null);
        assertEquals("获得角色总数时有问题",count1,count2);
    }

    @Test
    public void getRolesByParameter() throws Exception {
        RoleDO role = testUtil.createRole();
        RoleQuery param = new RoleQuery();
        param.setRoleId(role.getRoleId());
        List<RoleDO> list = dao.listRoleByParameter(param);
        assertEquals(1,list.size());
        assertEquals("根据角色id查询出的结果不一样",role.getRoleId(),list.get(0).getRoleId());
    }

    @Test
    public void getRolesByParameter2() throws Exception {
        RoleDO role = testUtil.createRole();
        RoleQuery param = new RoleQuery();
        param.addQuery(Query.equals("name",role.getName()));
        List<RoleDO> list = dao.listRoleByParameter(param);
        assertEquals(1,list.size());
        assertEquals("根据角色名称查询出的结果不一样",role.getRoleId(),list.get(0).getRoleId());
    }

    @Test
    public void getRolesByParameter3() throws Exception {
        //制造测试数据
        RoleDO role1 = testUtil.createRole();
        RoleDO role2 = testUtil.createRole();
        //调用测试方法
        RoleQuery param = new RoleQuery();
        List<String> roleId = Arrays.asList(role1.getRoleId(), role2.getRoleId());
        param.addQuery(Query.in("roleId", roleId));
        param.addSort("name", false);
        List<RoleDO> list = dao.listRoleByParameter(param);
        assertEquals(2, list.size());

        String sql = "SELECT role_0.* FROM xy_role AS role_0 WHERE roleId IN (:roleId) ORDER BY name DESC";
        NativeQuery query = dao.getSession().createNativeQuery(sql);
        query.setParameterList("roleId", roleId);
        query.addEntity("role_0", RoleDO.class);
        @SuppressWarnings("unchecked")
        List<RoleDO> list2 = query.list();
        assertEquals(2, list2.size());
        //判断结果和预期是否相同
        assertEquals("根据角色名称排序查询的结果不一样", list.get(0).getRoleId(), list2.get(0).getRoleId());
        assertEquals("根据角色名称排序查询的结果不一样", list.get(1).getRoleId(), list2.get(1).getRoleId());
    }

    @Test
    public void getRoleCountByUserId() throws Exception {
        RoleDO role = testUtil.createRole();
        Set<RoleDO> set = new HashSet<>();
        set.add(role);
        UserDO user = testUtil.createUser();
        user.setRoles(set);
        userDao.update(user);
        dao.getSession().flush();

        long count = dao.countRoleByUserId(user.getUserId(),null);
        assertEquals(1,count);
    }

    @Test
    public void getRolesByUserId() throws Exception {
        RoleDO role = testUtil.createRole();
        Set<RoleDO> set = new HashSet<>();
        set.add(role);
        UserDO user = testUtil.createUser();
        user.setRoles(set);
        userDao.update(user);
        dao.getSession().flush();

        List<RoleDO> list = dao.listRoleByUserId(user.getUserId(),null);
        assertEquals(1,list.size());
        assertEquals(role.getRoleId(),list.get(0).getRoleId());
    }

}