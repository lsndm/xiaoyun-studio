package studio.xiaoyun.core.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import studio.xiaoyun.core.exception.InvalidParameterException;
import studio.xiaoyun.core.pojo.FeedbackDO;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:config/spring-context-test.xml")
public class AbstractDaoImplTest {
	@Resource
    private FeedbackDaoImpl dao;
	@Resource
	private SessionFactory sessionFactory;
	
	private String createData(){
		FeedbackDO feedback = new FeedbackDO();
		feedback.setCreateDate(LocalDateTime.now());
		feedback.setText("text");
		feedback.setTitle("title");
		return dao.save(feedback);
	}
	
	@Test(expected = InvalidParameterException.class)
	public void testDelete(){
		String ID = createData();
		dao.delete(ID);
		dao.getById(ID);
	}

	@Test
	public void testGetByHQL() {
		String ID = createData();
		String hql = "from FeedbackDO where feedbackID='"+ID+"'";
		List<FeedbackDO> list = dao.listByHQL(hql,FeedbackDO.class);
		assertEquals(1,list.size());
		assertEquals(ID,list.get(0).getFeedbackId());
	}

	@Test
	public void testGetBySQL() {
		Session session = sessionFactory.getCurrentSession();
		FeedbackDO feedback = new FeedbackDO();
		feedback.setCreateDate(LocalDateTime.now());
		feedback.setText("text");
		feedback.setTitle("title");
		session.save(feedback);
		session.flush();
		String sql = "select title,text from xy_feedback where feedbackID='"+feedback.getFeedbackId()+"'";
		List<Object> list = dao.listBySQL(sql);
		assertEquals(1,list.size());
		Object[] objects = (Object[])list.get(0);
		assertEquals("title",objects[0]);
		assertEquals("text",objects[1]);
	}
	
	@Test
	public void testGetBySQL2() {
		Session session = sessionFactory.getCurrentSession();
		FeedbackDO feedback = new FeedbackDO();
		feedback.setCreateDate(LocalDateTime.now());
		feedback.setText("text");
		feedback.setTitle("title");
		session.save(feedback);
		session.flush();
		String sql = "select text from xy_feedback where feedbackID='"+feedback.getFeedbackId()+"'";
		List<Object> list = dao.listBySQL(sql);
		assertEquals(1,list.size());
		assertEquals("text",list.get(0).toString());
	}

	@Test
	public void testGetCountByHQL() {
		String ID = createData();
		String hql = "select count(*) from FeedbackDO where feedbackID='"+ID+"'";
		long count = dao.countByHQL(hql);
		assertEquals(1,count);
	}
	
	@Test
	public void testGetCountByHQL2() {
		String hql = "select count(*) from FeedbackDO where feedbackID=0";
		long count = dao.countByHQL(hql);
		assertEquals(0,count);
	}

	@Test
	public void testGetCountBySQL() {
		Session session = sessionFactory.getCurrentSession();
		FeedbackDO feedback = new FeedbackDO();
		feedback.setCreateDate(LocalDateTime.now());
		feedback.setText("text");
		feedback.setTitle("title");
		session.save(feedback);
		session.flush();
		String sql = "select count(*) from xy_feedback where feedbackID='"+feedback.getFeedbackId()+"'";
		long count = dao.countBySQL(sql);
		assertEquals(1,count);
	}

	@Test
	public void testGetEntityBySQL() {
		String ID = createData();
		String sql = "select a.* from xy_feedback a where a.feedbackID='"+ID+"'";
		List<FeedbackDO> list = dao.listBySQL(sql, "a",FeedbackDO.class);
		assertEquals(1,list.size());
		assertEquals(ID,list.get(0).getFeedbackId());
	}

	@Test
	public void testUpdate() {
		FeedbackDO feedback = new FeedbackDO();
		feedback.setCreateDate(LocalDateTime.now());
		feedback.setText("text");
		feedback.setTitle("title");
		dao.save(feedback);
		
		feedback.setText("aa");
		dao.update(feedback);
		
		FeedbackDO feedback2 = dao.getById(feedback.getFeedbackId());
		assertEquals(feedback.getText(),feedback2.getText());
	}

}
