package studio.xiaoyun.core.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import studio.xiaoyun.core.constant.UserStatus;
import studio.xiaoyun.core.pojo.UserDO;
import studio.xiaoyun.core.query.UserQuery;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:config/spring-context-test.xml")
public class UserDaoImplTest {
    @Resource
    private UserDaoImpl dao;
    @Resource
    private TestUtil testUtil;

    @Test
    public void getById() throws Exception {
        UserDO user = testUtil.createUser();
        UserDO user2 = dao.getById(user.getUserId());
        assertEquals(user.getUserId(), user2.getUserId());
    }

    @Test
    public void getUserByName() throws Exception {
        UserDO user = testUtil.createUser();
        Optional<UserDO> user2 = dao.getUserByName(user.getName());
        assertTrue(user2.isPresent());
        assertEquals(user.getUserId(), user2.get().getUserId());
    }

    @Test
    public void getUserByName2() throws Exception {
        Optional<UserDO> user2 = dao.getUserByName("9999999999999999");
        assertFalse(user2.isPresent());
    }

    @Test
    public void getUserByEmail() throws Exception {
        UserDO user = testUtil.createUser();
        Optional<UserDO> user2 = dao.getUserByEmail(user.getEmail());
        assertTrue(user2.isPresent());
        assertEquals(user.getUserId(), user2.get().getUserId());
    }

    @Test
    public void getUserByEmail2() throws Exception {
        Optional<UserDO> user2 = dao.getUserByEmail("99999999999999999");
        assertFalse(user2.isPresent());
    }

    @Test
    public void getUserCountByParameter() throws Exception {
        String sql = "select count(*) from xy_user where status ='" + UserStatus.NORMAL + "'";
        long count1 = dao.countBySQL(sql);
        long count2 = dao.countUserByParameter(null);
        assertEquals("获得用户总数时出错",count1,count2);
    }

    @Test
    public void getUserCountByParameter2() throws Exception {
        UserDO user = testUtil.createUser();
        UserQuery param = new UserQuery();
        param.setUserId(user.getUserId());
        long count = dao.countUserByParameter(param);
        assertEquals(1,count);
    }

    @Test
    public void getUsersByParameter() throws Exception {
        UserDO user = testUtil.createUser();
        UserQuery param = new UserQuery();
        param.setUserId(user.getUserId());
        List<UserDO> list = dao.listUserByParameter(param);
        assertEquals(1,list.size());
        assertEquals("根据用户id获得用户信息出错",user.getUserId(),list.get(0).getUserId());
    }

}