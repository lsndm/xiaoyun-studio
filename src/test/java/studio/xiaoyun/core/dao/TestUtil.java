package studio.xiaoyun.core.dao;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.mgt.SecurityManager;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import studio.xiaoyun.core.exception.XyException;
import studio.xiaoyun.core.constant.SystemUser;
import studio.xiaoyun.core.constant.UserStatus;
import studio.xiaoyun.core.constant.UserType;
import studio.xiaoyun.core.pojo.FeedbackDO;
import studio.xiaoyun.core.pojo.PermissionDO;
import studio.xiaoyun.core.pojo.RoleDO;
import studio.xiaoyun.core.pojo.UserDO;
import studio.xiaoyun.security.auth.UsernamePasswordToken;
import studio.xiaoyun.security.crypto.CipherService;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Repository
@Transactional
public class TestUtil {
    @Resource
    private SessionFactory sessionFactory;
    @Resource
    private CipherService cipherService;
    @Resource
    private SecurityManager securityManager;

    /**
     * 初始化shiro的安全管理器。
     * <p>如果测试时用到了shiro，需要调用该方法进行初始化</p>
     */
    public void initSecurityManager(){
        SecurityUtils.setSecurityManager(securityManager);
    }

    /**
     * 以管理员身份登陆
     */
    public void loginForAdmin(){
        initSecurityManager();
        if(!SecurityUtils.getSubject().isAuthenticated()){
            UsernamePasswordToken token = new UsernamePasswordToken(SystemUser.ADMIN.getName(), "1");
            token.setHost("127.0.0.1");
            token.setType(UserType.SYSTEM);
            SecurityUtils.getSubject().login(token);
        }
    }

    /**
     * 创建一个角色，每次创建的角色的名称和描述都不一样
     * @return 角色
     */
    public RoleDO createRole(){
        String uuid = UUID.randomUUID().toString().replaceAll("-","");
        RoleDO role = new RoleDO();
        role.setName(uuid);
        role.setDescription(uuid);
        sessionFactory.getCurrentSession().save(role);
        sessionFactory.getCurrentSession().flush();
        return role;
    }

    /**
     * 创建一个用户，每次创建的用户的名称都不一样
     * @return 用户
     */
    public UserDO createUser(){
        String uuid = UUID.randomUUID().toString().replaceAll("-","");
        UserDO user = new UserDO();
        user.setCreateDate(LocalDateTime.now());
        user.setName(uuid);
        user.setPassword(cipherService.encrypt("1"));
        user.setStatus(UserStatus.NORMAL);
        user.setUserType(UserType.USER);
        user.setEmail(uuid+"@xx.com");
        sessionFactory.getCurrentSession().save(user);
        sessionFactory.getCurrentSession().flush();
        return user;
    }

    /**
     * 从数据库中随机获得一条权限信息
     * @return 权限
     */
    public PermissionDO getPermission(){
        String sql = "select a.* from xy_permission as a order by rand()";
        NativeQuery query = sessionFactory.getCurrentSession().createNativeQuery(sql);
        query.setFirstResult(0);
        query.setMaxResults(1);
        query.addEntity("a",PermissionDO.class);
        @SuppressWarnings("unchecked")
        List<PermissionDO> list = query.list();
        if(list.size()==0){
            throw new XyException("数据库中没有权限信息");
        }else{
            return list.get(0);
        }
    }

    /**
     * 创建一个反馈，每次创建的反馈的名称和内容都不一样
     * @return 反馈
     */
    public FeedbackDO createFeedback(){
        String uuid = UUID.randomUUID().toString().replaceAll("-","");
        FeedbackDO feedback = new FeedbackDO();
        feedback.setCreateDate(LocalDateTime.now());
        feedback.setText(uuid);
        feedback.setTitle(uuid);
        sessionFactory.getCurrentSession().save(feedback);
        sessionFactory.getCurrentSession().flush();
        return feedback;
    }

}
