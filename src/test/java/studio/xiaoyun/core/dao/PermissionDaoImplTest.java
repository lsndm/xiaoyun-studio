package studio.xiaoyun.core.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import studio.xiaoyun.core.constant.Permission;
import studio.xiaoyun.core.pojo.PermissionDO;
import studio.xiaoyun.core.pojo.RoleDO;
import studio.xiaoyun.core.pojo.UserDO;
import studio.xiaoyun.core.query.PermissionQuery;

import javax.annotation.Resource;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:config/spring-context-test.xml")
public class PermissionDaoImplTest {
    @Resource
    private PermissionDaoImpl dao;
    @Resource
    private TestUtil testUtil;
    @Resource
    private UserDaoImpl userDao;
    @Resource
    private RoleDaoImpl roleDao;

    @Test
    public void getPermissionsByParameter() throws Exception {
        PermissionQuery param = new PermissionQuery();
        param.setName(Permission.FEEDBACK_DELETE_ALL);
        List<PermissionDO> list = dao.listPermissionByParameter(param);
        assertEquals(1,list.size());
        assertEquals("查询出的结果和预期不同",Permission.FEEDBACK_DELETE_ALL,list.get(0).getName());
    }

    @Test
    public void getPermissionCountByParameter() throws Exception {
        PermissionQuery param = new PermissionQuery();
        param.setName(Permission.FEEDBACK_DELETE_ALL);
        long count = dao.countPermissionByParameter(param);
        assertEquals(1,count);
    }

    @Test
    public void getPermissionsByUserId() throws Exception {
        PermissionDO permission = testUtil.getPermission();
        Set<PermissionDO> set = new HashSet<>();
        set.add(permission);
        UserDO user = testUtil.createUser();
        user.setPermissions(set);
        userDao.update(user);
        dao.getSession().flush();  //将修改提交到数据库

        List<PermissionDO> list = dao.listPermissionByUserId(user.getUserId(),null);
        assertEquals(1,list.size());
        assertEquals(permission.getName(),list.get(0).getName());
    }

    @Test
    public void getPermissionCountByUserId() throws Exception {
        PermissionDO permission = testUtil.getPermission();
        Set<PermissionDO> set = new HashSet<>();
        set.add(permission);
        UserDO user = testUtil.createUser();
        user.setPermissions(set);
        userDao.update(user);
        dao.getSession().flush();  //将修改提交到数据库

        long count = dao.countPermissionByUserId(user.getUserId(),null);
        assertEquals(1,count);
    }

    @Test
    public void getPermissionsByRoleId() throws Exception {
        PermissionDO permission = testUtil.getPermission();
        Set<PermissionDO> set = new HashSet<>();
        set.add(permission);
        RoleDO role = testUtil.createRole();
        role.setPermissions(set);
        roleDao.update(role);
        dao.getSession().flush();  //将修改提交到数据库

        List<PermissionDO> list = dao.listPermissionByRoleId(role.getRoleId(),null);
        assertEquals(1,list.size());
        assertEquals(permission.getPermissionId(),list.get(0).getPermissionId());
    }

    @Test
    public void getPermissionCountByRoleId() throws Exception {
        PermissionDO permission = testUtil.getPermission();
        Set<PermissionDO> set = new HashSet<>();
        set.add(permission);
        RoleDO role = testUtil.createRole();
        role.setPermissions(set);
        roleDao.update(role);
        dao.getSession().flush();  //将修改提交到数据库

        long count = dao.countPermissionByRoleId(role.getRoleId(),null);
        assertEquals(1,count);
    }

}