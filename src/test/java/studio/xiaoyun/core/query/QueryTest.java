package studio.xiaoyun.core.query;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import studio.xiaoyun.core.query.criterion.Query;

public class QueryTest {

	@Test
	public void testGetFirstResult() {
		TestQuery param = new TestQuery();
		param.setFirstResult(1);
		int n = param.getFirstResult();
		assertEquals(1,n);
	}

	@Test
	public void testGetIncludeField() {
		TestQuery param = new TestQuery();
		param.addIncludeField("ID","name");
		List<String> list = param.getIncludeField();
		assertTrue(list.contains("ID"));
		assertTrue(list.contains("name"));
	}

	@Test
	public void testGetIsAsc() {
		TestQuery param = new TestQuery();
		param.addSort("name", true);
		param.addSort("ID", false);
		List<Boolean> list = param.getIsAsc();
		assertTrue(list.get(0));
		assertFalse(list.get(1));
	}

	@Test
	public void testGetMaxResults() {
		TestQuery param = new TestQuery();
		param.setMaxResults(10);
		int m = param.getMaxResults();
		assertEquals(10,m);
	}

	@Test
	public void testGetQuery() {
		TestQuery param = new TestQuery();
		param.addQuery(Query.equals("name", "a"));
		String query = param.getQuery();
		assertEquals("name = ?",query);
	}

	@Test
	public void testGetQueryMapOfStringString() {
		TestQuery param = new TestQuery();
		param.addQuery(Query.equals("name", "a"));
		Map<String,String> map = new HashMap<>();
		map.put("name", "Name");
		String query = param.getQuery(map);
		assertEquals("Name = ?",query);
	}

	@Test
	public void testGetSortField() {
		TestQuery param = new TestQuery();
		param.addSort("name", true);
		param.addSort("ID", false);
		List<String> list = param.getSortField();
		assertEquals("name",list.get(0));
		assertEquals("ID",list.get(1));
	}

	@Test
	public void testValidate() {
		TestQuery param = new TestQuery();
		param.addSort("ddd", true);
		boolean flag = param.validate();
		assertFalse(flag);
	}
	
	@Test
	public void testValidate2() {
		TestQuery param = new TestQuery();
		param.addIncludeField("ddd");
		boolean flag = param.validate();
		assertFalse(flag);
	}
	
	@Test
	public void testValidate3() {
		TestQuery param = new TestQuery();
		param.addQuery(Query.equals("dd", "aa"));
		boolean flag = param.validate();
		assertFalse(flag);
	}
	
	@Test
	public void testGetQuery4() {
		TestQuery test = new TestQuery();
		test.addQuery(Query.and(Query.ge("ID1", 1),Query.le("ID1", 3)));
		test.addQuery(Query.or(Query.ge("ID2", 1),Query.le("ID2", 3)));
		String query = test.getQuery();
		assertEquals("(ID1 >= ? and ID1 <= ?) and (ID2 >= ? or ID2 <= ?)",query);
	}
	
	@Test
	public void testGetQuery5() {
		TestQuery test = new TestQuery();
		test.addQuery(Query.ge("ID1", 1));
		test.addQuery(Query.le("ID1", 3));
		test.addQuery(Query.or(Query.ge("ID2", 1),Query.le("ID2", 3)));
		String query = test.getQuery();
		assertEquals("ID1 >= ? and ID1 <= ? and (ID2 >= ? or ID2 <= ?)",query);
	}

	@Test
	public void testGetDefaultValue(){
		TestQuery2 test = new TestQuery2();
		test.addQuery(Query.equals("name","b"));
		String str = test.getQuery();
		List<Object> list = test.getQueryValue();
		assertEquals("name = ?",str);
		assertEquals("b",list.get(0));
	}

	@Test
	public void testGetDefaultValue2(){
		TestQuery2 test = new TestQuery2();
		test.addQuery(Query.equals("userId","1"));
		String str = test.getQuery();
		List<Object> list = test.getQueryValue();
		assertEquals("userId = ? and name = ?",str);
		assertEquals("1",list.get(0));
		assertEquals("a",list.get(1));
	}

	@Test
	public void testGetDefaultValue3(){
		TestQuery2 test = new TestQuery2();
		test.addQuery(Query.and(Query.equals("userId",1),Query.or(Query.equals("name","c"),Query.equals("name","d"))));
		String str = test.getQuery();
		List<Object> list = test.getQueryValue();
		assertEquals("userId = ? and (name = ? or name = ?)",str);
		assertEquals("1",list.get(0).toString());
		assertEquals("c",list.get(1).toString());
		assertEquals("d",list.get(2).toString());
	}

}
