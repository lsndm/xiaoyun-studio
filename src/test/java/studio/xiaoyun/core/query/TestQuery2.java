package studio.xiaoyun.core.query;

import studio.xiaoyun.core.query.criterion.AbstractCriterion;
import studio.xiaoyun.core.query.criterion.Query;

import java.util.List;

public class TestQuery2 extends AbstractQuery {
    private String name;
    private Integer userId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    List<AbstractCriterion> getDefaultValue() {
        List<AbstractCriterion> list = super.getDefaultValue();
        list.add(Query.equals("name","a"));
        return list;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
