package studio.xiaoyun.core.query.criterion;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import studio.xiaoyun.core.query.TestQuery;
import studio.xiaoyun.core.query.criterion.AbstractCriterion;
import studio.xiaoyun.core.query.criterion.Query;

public class CriterionOrTest {

	@Test
	public void testGetPropertyName() {
		AbstractCriterion c = Query.or(Query.ge("name", "1"),Query.gt("ID1", "aa"));
		List<String> name = c.getPropertyName();
		assertEquals("name", name.get(0));
		assertEquals("ID1", name.get(1));
	}

	@Test
	public void testGetQuery1() {
		TestQuery test = new TestQuery();
		test.addQuery(Query.or(Query.ge("ID1", 1),Query.gt("ID2", 2)));
		String query = test.getQuery();
		assertEquals("ID1 >= ? or ID2 > ?",query);
	}
	
	@Test
	public void testGetQuery2() {
		TestQuery test = new TestQuery();
		test.addQuery(Query.or(Query.ge("ID1", 1),Query.and(Query.gt("ID2", 2),Query.like("name", "a"))));
		String query = test.getQuery();
		assertEquals("ID1 >= ? or (ID2 > ? and name like ?)",query);
	}

	@Test
	public void getPropertyValue(){
		TestQuery test = new TestQuery();
		test.addQuery(Query.or(Query.ge("ID1", 1),Query.gt("ID2", 2)));
		List<Object> list = test.getQueryValue();
		assertEquals("1",list.get(0).toString());
		assertEquals("2",list.get(1).toString());
	}

}
