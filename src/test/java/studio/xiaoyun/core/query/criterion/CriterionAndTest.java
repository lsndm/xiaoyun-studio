package studio.xiaoyun.core.query.criterion;

import org.junit.Test;
import studio.xiaoyun.core.query.TestQuery;
import studio.xiaoyun.core.query.criterion.AbstractCriterion;
import studio.xiaoyun.core.query.criterion.Query;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class CriterionAndTest {

	@Test
	public void testGetPropertyName() {
		AbstractCriterion c = Query.and(Query.ge("name", "1"),Query.gt("ID1", "aa"));
		List<String> name = c.getPropertyName();
		assertEquals("name", name.get(0));
		assertEquals("ID1", name.get(1));
	}

	@Test
	public void testGetQuery1() {
		TestQuery test = new TestQuery();
		test.addQuery(Query.and(Query.ge("ID1", 1),Query.gt("ID2", 2)));
		String query = test.getQuery();
		assertEquals("ID1 >= ? and ID2 > ?",query);
	}
	
	@Test
	public void testGetQuery2() {
		TestQuery test = new TestQuery();
		test.addQuery(Query.and(Query.ge("ID1", 1),Query.or(Query.gt("ID2", 2),Query.like("name", "a"))));
		String query = test.getQuery();
		assertEquals("ID1 >= ? and (ID2 > ? or name like ?)",query);
	}
	
	@Test
	public void testGetQuery3() {
		TestQuery test = new TestQuery();
		test.addQuery(Query.and(Query.ge("ID1", 1),Query.le("ID1", 3)));
		test.addQuery(Query.and(Query.ge("ID2", 1),Query.le("ID2", 3)));
		String query = test.getQuery();
		assertEquals("(ID1 >= ? and ID1 <= ?) and (ID2 >= ? and ID2 <= ?)",query);
	}

	@Test
	public void getPropertyValue(){
		AbstractCriterion c = Query.and(Query.ge("name", "1"),Query.gt("ID1", "aa"));
		List<Object> list = c.getPropertyValue(TestQuery.class);
		assertEquals(2,list.size());
		assertEquals(String.class,list.get(0).getClass());
		assertEquals(String.class,list.get(1).getClass());
	}

	@Test
	public void getPropertyValue2(){
		AbstractCriterion c = Query.and(Query.ge("name", "1"),Query.gt("ID1", "aa"));
		List<Object> list = c.getPropertyValue(TestQuery.class);
		assertEquals("1",list.get(0));
		assertEquals("aa",list.get(1));
	}
	
}
