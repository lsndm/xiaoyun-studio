package studio.xiaoyun.core.query.criterion;

import org.junit.Test;
import studio.xiaoyun.core.query.criterion.AbstractCriterion;
import studio.xiaoyun.core.query.criterion.Query;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class CriterionInTest {
    @Test
    public void getQuery() throws Exception {
        AbstractCriterion c = Query.in("name", Arrays.asList("a","b","c"));
        String str = c.getQuery();
        assertEquals("%s in (?,?,?)",str);
    }

    @Test
    public void getQuery2() throws Exception {
        AbstractCriterion c = Query.in("name", Arrays.asList("a"));
        String str = c.getQuery();
        assertEquals("%s in (?)",str);
    }

}