package studio.xiaoyun.core.query.criterion;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.Test;
import studio.xiaoyun.core.exception.XyException;
import studio.xiaoyun.core.query.TestQuery;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class CriterionTest {
    @Test
    public void getPropertyValue() throws Exception {
        AbstractCriterion c = Query.equals("name","aa");
        Object v = c.getPropertyValue(TestQuery.class,"name","aa");
        assertEquals(String.class,v.getClass());
    }

    @Test(expected = XyException.class)
    public void getPropertyValue2() throws Exception {
        AbstractCriterion c = Query.equals("name","aa");
        c.getPropertyValue(TestQuery.class,"name1","aa");
    }

    @Test
    public void getPropertyValue3() throws Exception {
        AbstractCriterion c = Query.equals("name", "aa");
        Object v = c.getPropertyValue(TestQuery.class, "ID1", 1);
        assertEquals("1", v.toString());
    }

    @Test
    public void getPropertyValue4() throws Exception {
        AbstractCriterion c = Query.equals("name", "aa");
        Object v = c.getPropertyValue(TestQuery.class, "enum1", TestEnum1.A);
        assertEquals(String.class, v.getClass());
        assertEquals("A", v.toString());
    }

    @Test
    public void getPropertyValue5() throws Exception {
        AbstractCriterion c = Query.equals("name","aa");
        Object v = c.getPropertyValue(TestQuery.class,"date",new Date());
        assertEquals(Date.class,v.getClass());
    }

    @Test
    public void getPropertyValue6() throws Exception {
        String dateStr = "2010-02-02";
        AbstractCriterion c = Query.equals("name","aa");
        Object v = c.getPropertyValue(TestQuery.class,"date",dateStr);
        assertEquals(Date.class,v.getClass());
        assertEquals(dateStr, DateFormatUtils.format((Date)v,"yyyy-MM-dd"));
    }

    @Test
    public void getPropertyValue7() throws Exception {
        String dateStr = "2010-02-02 20:02:30";
        AbstractCriterion c = Query.equals("name","aa");
        Object v = c.getPropertyValue(TestQuery.class,"date",dateStr);
        assertEquals(Date.class,v.getClass());
        assertEquals(dateStr, DateFormatUtils.format((Date)v,"yyyy-MM-dd HH:mm:ss"));
    }

    @Test
    public void getPropertyValue8() throws Exception {
        String dateStr = "2010-02-02 20:02:30";
        AbstractCriterion c = Query.equals("name","aa");
        Object v = c.getPropertyValue(TestQuery.class,"localDateTime",dateStr);
        assertEquals(LocalDateTime.class,v.getClass());
        assertEquals(dateStr,((LocalDateTime)v).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
    }

    @Test
    public void getPropertyValue9() throws Exception {
        String dateStr = "2010-02-02";
        AbstractCriterion c = Query.equals("name","aa");
        Object v = c.getPropertyValue(TestQuery.class,"localDate",dateStr);
        assertEquals(LocalDate.class,v.getClass());
        assertEquals(dateStr,((LocalDate)v).format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
    }

    @Test
    public void getPropertyValue10() throws Exception {
        String dateStr = "10:20:10";
        AbstractCriterion c = Query.equals("name","aa");
        Object v = c.getPropertyValue(TestQuery.class,"localTime",dateStr);
        assertEquals(LocalTime.class,v.getClass());
        assertEquals(dateStr,((LocalTime)v).format(DateTimeFormatter.ofPattern("HH:mm:ss")));
    }
}