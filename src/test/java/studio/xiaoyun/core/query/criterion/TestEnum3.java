package studio.xiaoyun.core.query.criterion;

public enum TestEnum3 {
	A("a"), B("b"), C("c");
	private String i;

	TestEnum3(String i) {
		this.i = i;
	}

	public String value() {
		return i;
	}
}
