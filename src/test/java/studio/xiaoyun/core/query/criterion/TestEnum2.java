package studio.xiaoyun.core.query.criterion;

public enum TestEnum2 {
	A(1), B(2), C(3);
	private int i;

	TestEnum2(int i) {
		this.i = i;
	}

	public int value() {
		return i;
	}

}
