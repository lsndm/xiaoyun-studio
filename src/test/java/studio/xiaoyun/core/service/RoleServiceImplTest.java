package studio.xiaoyun.core.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import studio.xiaoyun.core.constant.SystemRole;
import studio.xiaoyun.core.dao.PermissionDao;
import studio.xiaoyun.core.dao.RoleDao;
import studio.xiaoyun.core.dao.UserDao;
import studio.xiaoyun.core.dao.TestUtil;
import studio.xiaoyun.core.pojo.PermissionDO;
import studio.xiaoyun.core.pojo.RoleDO;
import studio.xiaoyun.core.pojo.UserDO;
import studio.xiaoyun.core.exception.InvalidParameterException;
import studio.xiaoyun.core.query.PermissionQuery;

import javax.annotation.Resource;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:config/spring-context-test.xml")
public class RoleServiceImplTest {
    @Resource
    private RoleService roleService;
    @Resource
    private TestUtil testUtil;
    @Resource
    private PermissionDao permissionDao;
    @Resource
    private SessionFactory sessionFactory;
    @Resource
    private RoleDao roleDao;
    @Resource
    private UserDao userDao;

    @Test
    public void setRolePermission() throws Exception {
        RoleDO role = testUtil.createRole();
        PermissionQuery param = new PermissionQuery();
        param.setMaxResults(1);
        List<PermissionDO> list = permissionDao.listPermissionByParameter(param);
        List<String> permissionSet = list.stream().map(PermissionDO::getPermissionId).collect(Collectors.toList());
        roleService.setRolePermission(role.getRoleId(),permissionSet);

        String sql = "select permissionId from xy_role_permission where roleId='"+role.getRoleId()+"'";
        Session session = sessionFactory.getCurrentSession();
        session.flush();
        Query query = session.createNativeQuery(sql);
        @SuppressWarnings("unchecked")
        List<String> list2 = query.list();
        assertEquals(1,list2.size());
        assertEquals(list.get(0).getPermissionId(),list2.get(0));
    }

    @Test(expected = InvalidParameterException.class)
    public void deleteRole() throws Exception {
        RoleDO role = testUtil.createRole();
        UserDO user = testUtil.createUser();
        user.setRoles(Collections.singleton(role));
        userDao.update(user);
        roleService.deleteRole(Collections.singletonList(role.getRoleId()));
        long count = roleDao.countRoleByUserId(user.getUserId(),null);
        assertEquals(0,count);
        roleDao.getById(role.getRoleId());
    }

    @Test
    public void updateRole() throws Exception {
        RoleDO role = testUtil.createRole();
        role.setName("aa");
        role.setDescription("bb");
        roleService.updateRole(role);
        RoleDO role2 = roleDao.getById(role.getRoleId());
        assertEquals("aa",role2.getName());
        assertEquals("bb",role2.getDescription());
    }

    @Test(expected = InvalidParameterException.class)
    public void updateRole2() throws Exception {
        RoleDO role = roleDao.getById(SystemRole.ADMIN.getRoleId());
        role.setName("aa");
        roleService.updateRole(role);
    }

    @Test
    public void createRole() throws Exception {
        String roleId = roleService.saveRole("aaddd","bbb");
        RoleDO role = roleDao.getById(roleId);
        assertEquals("aaddd",role.getName());
        assertEquals("bbb",role.getDescription());
    }
}