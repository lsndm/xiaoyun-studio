package studio.xiaoyun.security.auth;

import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import studio.xiaoyun.core.constant.SystemUser;
import studio.xiaoyun.core.constant.UserStatus;
import studio.xiaoyun.core.constant.UserType;
import studio.xiaoyun.core.dao.TestUtil;
import studio.xiaoyun.core.dao.UserDaoImpl;
import studio.xiaoyun.core.pojo.UserDO;
import studio.xiaoyun.security.crypto.CipherService;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:config/spring-context-test.xml")
public class AuthRealmTest {
    @Resource
    private TestUtil testUtil;
    @Resource
    private AuthRealm authRealm;
    @Resource
    private UserDaoImpl userDao;
    @Resource
    private CipherService cipherService;

    /**
     * 用户名或邮箱找不到时应该抛出异常
     */
    @Test(expected = UnknownAccountException.class)
    public void testUnknownAccount() throws Exception {
        UsernamePasswordToken token =  new UsernamePasswordToken("999999999999","ssssssssss");
        token.setType(UserType.USER);
        authRealm.doGetAuthenticationInfo(token);
    }

    /**
     * 用户已删除时应该抛出异常
     */
    @Test(expected = DisabledAccountException.class)
    public void testDeletedUser(){
        String uuid = UUID.randomUUID().toString().replaceAll("-","");
        UserDO user = new UserDO();
        user.setCreateDate(LocalDateTime.now());
        user.setName(uuid);
        user.setPassword(cipherService.encrypt("1"));
        user.setStatus(UserStatus.DELETED);
        user.setUserType(UserType.USER);
        user.setEmail("22222222222@ss.com");
        userDao.save(user);
        UsernamePasswordToken token =  new UsernamePasswordToken(user.getName(),"ssssssssss");
        token.setType(UserType.USER);
        authRealm.doGetAuthenticationInfo(token);
    }

    /**
     * 当管理员尝试以普通用户身份登陆时应该抛出异常
     */
    @Test(expected = UnknownAccountException.class)
    public void testErrorUserType(){
        UsernamePasswordToken token =  new UsernamePasswordToken(SystemUser.ADMIN.getName(),"ssssssssss");
        token.setType(UserType.USER);
        authRealm.doGetAuthenticationInfo(token);
    }

    /**
     * 当普通用户尝试以管理员身份登陆时应该抛出异常
     */
    @Test(expected = UnknownAccountException.class)
    public void testErrorUserType2(){
        UserDO user = testUtil.createUser();
        UsernamePasswordToken token =  new UsernamePasswordToken(user.getName(),"ssssssssss");
        token.setType(UserType.SYSTEM);
        authRealm.doGetAuthenticationInfo(token);
    }

    /**
     * 以用户名登陆
     */
    @Test
    public void loginByName(){
        UserDO user = testUtil.createUser();
        UsernamePasswordToken token =  new UsernamePasswordToken(user.getName(),"ssssssssss");
        token.setType(UserType.USER);
        SimpleAuthenticationInfo info = (SimpleAuthenticationInfo)authRealm.doGetAuthenticationInfo(token);
        assertEquals(user.getUserId(),info.getPrincipals().iterator().next());
    }

    /**
     * 以邮箱登陆
     */
    @Test
    public void loginByEmail(){
        UserDO user = testUtil.createUser();
        UsernamePasswordToken token =  new UsernamePasswordToken(user.getEmail(),"ssssssssss");
        token.setType(UserType.USER);
        SimpleAuthenticationInfo info = (SimpleAuthenticationInfo)authRealm.doGetAuthenticationInfo(token);
        assertEquals(user.getUserId(),info.getPrincipals().iterator().next());
    }

}