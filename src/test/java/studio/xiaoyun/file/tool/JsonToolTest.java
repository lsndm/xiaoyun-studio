package studio.xiaoyun.file.tool;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;
import studio.xiaoyun.core.SpringConfig;
import studio.xiaoyun.core.constant.UserType;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class JsonToolTest {
    @Test
    public void strToJson(){
        TestJson json = new TestJson();
        json.setStr("a");
        json.setStr2("b");
        String str = JSON.toJSONString(json);
        assertEquals("{\"str\":\"a\",\"str2\":\"b\"}",str);
    }

    @Test
    public void dateToJson() throws ParseException {
        TestJson json = new TestJson();
        json.setDate(DateUtils.parseDate("2010-01-01 10:10:10","yyyy-MM-dd HH:mm:ss"));
        String str = JSON.toJSONString(json, SpringConfig.getFastJsonSerializeConfig());
        assertEquals("{\"date\":\"2010-01-01 10:10:10\"}",str);
    }

    @Test
    public void localDateTimeToJson(){
        TestJson json = new TestJson();
        json.setLocalDateTime(LocalDateTime.now());
        String str = JSON.toJSONString(json, SpringConfig.getFastJsonSerializeConfig());
        String timeStr = json.getLocalDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        assertEquals("{\"localDateTime\":\""+timeStr+"\"}",str);
    }

    @Test
    public void localDateToJson(){
        TestJson json = new TestJson();
        json.setLocalDate(LocalDate.now());
        String str = JSON.toJSONString(json, SpringConfig.getFastJsonSerializeConfig());
        String timeStr = json.getLocalDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        assertEquals("{\"localDate\":\""+timeStr+"\"}",str);
    }

    @Test
    public void localTimeToJson(){
        TestJson json = new TestJson();
        json.setLocalTime(LocalTime.now());
        String str = JSON.toJSONString(json, SpringConfig.getFastJsonSerializeConfig());
        String timeStr = json.getLocalTime().format(DateTimeFormatter.ofPattern("HH:mm:ss"));
        assertEquals("{\"localTime\":\""+timeStr+"\"}",str);
    }

    @Test
    public void arrayToJson(){
        TestJson json = new TestJson();
        json.setArray(new String[]{"a","b"});
        String str = JSON.toJSONString(json, SpringConfig.getFastJsonSerializeConfig());
        assertEquals("{\"array\":[\"a\",\"b\"]}",str);
    }

    @Test
    public void enumToJson(){
        TestJson json = new TestJson();
        json.setUserType(UserType.USER);
        String str = JSON.toJSONString(json, SpringConfig.getFastJsonSerializeConfig());
        assertEquals("{\"userType\":\""+UserType.USER.name()+"\"}",str);
    }

    @Test
    public void intToJson(){
        TestJson2 json = new TestJson2();
        json.setaDouble(1.1);
        json.setaInt(2);
        json.setaLong(3);
        String str = JSON.toJSONString(json, SpringConfig.getFastJsonSerializeConfig());
        assertEquals("{\"aDouble\":1.1,\"aInt\":2,\"aLong\":3}",str);
    }

    @Test
    public void jsonToString(){
        TestJson json = JSON.parseObject("{\"str2\":\"b\",\"str\":\"a\"}",TestJson.class);
        assertEquals("a",json.getStr());
        assertEquals("b",json.getStr2());
    }

    @Test
    public void jsonToDate(){
        TestJson json = JSON.parseObject("{\"date\":\"2016-01-01 10:20:20\"}",TestJson.class);
        assertEquals("2016-01-01 10:20:20", DateFormatUtils.format(json.getDate(),"yyyy-MM-dd HH:mm:ss"));
        json = JSON.parseObject("{\"date\":\"2016-01-01\"}",TestJson.class);
        assertEquals("2016-01-01 00:00:00", DateFormatUtils.format(json.getDate(),"yyyy-MM-dd HH:mm:ss"));
    }

    @Test
    public void jsonToLocalDateTime(){
        TestJson json = JSON.parseObject("{\"localDateTime\":\"2016-01-01 10:20:20\"}",TestJson.class);
        assertEquals(LocalDateTime.of(2016,1,1,10,20,20),json.getLocalDateTime());
    }

    @Test
    public void jsonToLocalDate(){
        TestJson json = JSON.parseObject("{\"localDate\":\"2016-01-01\"}",TestJson.class);
        assertEquals(LocalDate.of(2016,1,1),json.getLocalDate());
    }

    @Test
    public void jsonToLocalTime(){
        TestJson json = JSON.parseObject("{\"localTime\":\"10:20:30\"}",TestJson.class);
        assertEquals(LocalTime.of(10,20,30),json.getLocalTime());
    }

    @Test
    public void jsonToMap(){
        Map map = JSON.parseObject("{\"str2\":\"b\",\"str\":\"a\"}",Map.class);
        assertEquals("a",map.get("str"));
        assertEquals("b",map.get("str2"));
    }

    @Test
    public void jsonToList(){
        List list = JSON.parseObject("[\"a\",\"b\"]",List.class);
        assertEquals(2,list.size());
        assertTrue(list.contains("a"));
        assertTrue(list.contains("b"));
    }

    @Test
    public void jsonToSet(){
        Set list = JSON.parseObject("[\"a\",\"b\",\"b\"]",Set.class);
        assertEquals(2,list.size());
        assertTrue(list.contains("a"));
        assertTrue(list.contains("b"));
    }

    @Test
    public void jsonToArray(){
        TestJson json1 = new TestJson();
        json1.setStr("a");
        json1.setStr2("b");
        TestJson json2 = new TestJson();
        json2.setStr("c");
        json2.setStr2("d");
        List<TestJson> list = new ArrayList<>();
        list.add(json1);
        list.add(json2);
        String str = JSON.toJSONString(list);
        List<TestJson> list2 = JSON.parseArray(str,TestJson.class);
        assertEquals(2,list2.size());
        assertEquals("a",list2.get(0).getStr());
        assertEquals("b",list2.get(0).getStr2());
        assertEquals("c",list2.get(1).getStr());
        assertEquals("d",list2.get(1).getStr2());
    }

    @Test
    public void jsonToArray2(){
        Map<String,String> map = new HashMap<>();
        map.put("str","a");
        map.put("str2","b");
        List<Map<String,String>> list = new ArrayList<>();
        list.add(map);
        String str = JSON.toJSONString(list);
        List<Map> list2 = JSON.parseArray(str,Map.class);
        assertEquals("a",list2.get(0).get("str").toString());
        assertEquals("b",list2.get(0).get("str2").toString());
    }

}