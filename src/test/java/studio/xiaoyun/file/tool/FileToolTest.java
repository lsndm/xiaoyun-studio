package studio.xiaoyun.file.tool;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.charset.Charset;
import java.util.Optional;

import org.junit.Test;
import studio.xiaoyun.file.tool.FileTool;

public class FileToolTest {

    @Test
    public void getFileExtension() throws Exception {
        String fileName = "test.doc";
        String extension = FileTool.getFileExtension(fileName);
        assertEquals("doc",extension);
    }

    @Test
    public void getFileExtension2() throws Exception {
        String fileName = "test";
        String extension = FileTool.getFileExtension(fileName);
        assertEquals("",extension);
    }

    @Test
    public void getFileExtension3() throws Exception {
        String fileName = "test.DOC";
        String extension = FileTool.getFileExtension(fileName);
        assertEquals("doc",extension);
    }
    
}