package studio.xiaoyun.file.converter;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import studio.xiaoyun.file.tool.PoiTool;

public class IHtmlConverterTest {

	@Test
	public void testIsSupportedExtensions() {
		IHtmlConverter converter = new PoiTool();
		converter.setSupportedExtensionsForHtml(Arrays.asList("doc","DOCX",""));
		assertTrue(converter.isSupportedExtensionsForHtml("doc"));
		assertTrue(converter.isSupportedExtensionsForHtml("docx"));
		assertTrue(converter.isSupportedExtensionsForHtml(""));
		assertFalse(converter.isSupportedExtensionsForHtml("d"));
	}

}
