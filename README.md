# 小云工作室
主页兼项目演示效果：[www.xiaon.org](http://www.xiaon.org)

### 重要说明
本项目已经拆分为前端项目[xiaoyun-web](http://git.oschina.net/xiaoyun_studio/xiaoyun-web)和后端项目[xiaoyun-core](http://git.oschina.net/xiaoyun_studio/xiaoyun-core) ，之后本项目将不再更新，新的两个项目仍然持续更新，拆分理由如下：
* 我很认同前端和后端完全分离的想法，在开始做本项目的时候，我就有这个想法，但是并没有找到合适的办法，现在时机终于成熟
* 后端微服务、分布式架构开始流行
* 前端技术的飞速发展，像vue、angular、node等的流行

### 目前实现的功能
 * 权限控制：控制粒度精细到每个web api，管理员可以在管理控制台给用户、角色赋予权限
 * rest风格的web api：根据http方法决定增、删、改、查，返回值支持json和xml，请求成功或失败会返回相应的http响应码，成功是2xx，失败是4xx或者5xx
 * 事件驱动
 * 前端使用bootstrap，使网页可以自适应用户的设备
 * 使用adminlte为模板开发的后台管理界面
 * office转pdf(支持wsoffice,wps,openoffice)，office转html(支持wsoffice,wps,openoffice,poi)

### 以后的计划
 * 作为项目主页，展示本项目相关的信息
 * 作为个人博客，展示个人的文章

### 使用教程
1. 使用git或者svn下载源码，也可以在本页面直接下载zip压缩包
2. 命令行中进入项目根目录，然后执行命令： mvn jetty:run 。maven会自动下载jar，编译源码，然后在jetty中运行项目。项目的所有配置都提供了默认值，
不需要修改就可以运行。数据库默认使用h2，系统启动时会自动创建数据库、表，自动导入用户、权限等初始数据。
3. 浏览器输入http://localhost:8080  就可以看到首页，后台登陆地址是：http://localhost:8080/other/alogin  ,用户名是admin，密码是1

由于maven中央仓库速度很慢，分享一个阿里巴巴的maven仓库，速度非常快。在线搜索地址：[maven.aliyun.com](http://maven.aliyun.com/nexus/),
仓库地址：http://maven.aliyun.com/nexus/content/groups/public/ ，修改仓库地址的方法：找到C:\Users\用户名\ .m2\setting.xml，将文件中注释掉的mirrors取消注释，
并修改为：
```
<mirrors>
    <mirror>
      <id>alimaven</id>
      <name>aliyun maven</name>
      <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
      <mirrorOf>central</mirrorOf>
    </mirror>
  </mirrors>
```

### 界面截图
 ![首页](http://git.oschina.net/uploads/images/2017/0211/164125_155f0f5a_810614.png "首页")

 首页

 ![后台角色管理界面](http://git.oschina.net/uploads/images/2017/0211/164157_d5125f08_810614.png "后台角色管理界面")

 后台角色管理界面

### 技术框架
 * 核心框架：spring 4
 * MVC框架：spring mvc 4
 * 持久层：hibernate 5
 * 安全框架：shiro
 * 日志管理：logback
 * 数据库：h2、mysql、oracle
 * js框架：jquery 3
 * css框架：bootstrap 3
 * 单元测试：junit 4
 
### 欢迎加入
 小云工作室期待您的加入！！！